﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data.SqlClient;


namespace Potal_Praca.Register
{
    public partial class Register_Activate : System.Web.UI.Page
    {
        SqlConnection con;


        protected void Page_Load(object sender, EventArgs e)
        {
            con = new SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings["Portal_pracaConnectionString"].ConnectionString);

            try
            {
                if (Request["token"] == null)
                {
                    Response.Redirect("~/Default.aspx");
                }

                con.Open();
                this.Validator(sender, e);

                SqlCommand command = new SqlCommand("UPDATE Logins SET Active=1 WHERE Token LIKE '" + Request["token"].ToString() + "'", con);


                command.ExecuteNonQuery();

                Activate_Label.Text = "Twoje konto zostało aktywowane. <br />" +
                                         "Możesz się teraz <a href=\"../Account/Login.aspx\">zalogować</a>.<br />";

                con.Close();

            }
            catch (Exception a)
            {
                Activate_Label.ForeColor = System.Drawing.ColorTranslator.FromHtml("red");
                Activate_Label.Text = a.Message;
            }
            finally
            {
                con.Close();
            }


        }

        protected void Validator(object sender, EventArgs e)
        {
            SqlCommand command = new SqlCommand("SELECT * FROM Logins WHERE Token LIKE '" + Request["token"].ToString() + "'", con);

            if (command.ExecuteScalar() == null)
            {
                throw new Exception("Link aktywacyjny jest nieprawidłowy.");
            }


        }


    }
}