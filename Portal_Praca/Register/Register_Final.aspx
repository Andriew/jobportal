﻿<%@ Page Title="Rejestracja Firmy lub Spółki" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" 
CodeBehind="Register_Final.aspx.cs" Inherits="Potal_Praca.Register.Register_Final" %>

<asp:Content ID="HeaderContent" runat="server" ContentPlaceHolderID="HeadContent">

 <style type="text/css">
        #Label_Align
        {
           text-align: center;
           font-size: large;
           margin-left: 100px;
        }
 </style>

</asp:Content>

<asp:Content ID="Body" runat="server" contentplaceholderid="MainContent">
<div id="Label_Align">
<br />

<asp:Label ID="Register_Finish_Message" runat="server"  Text="Twoje konto zostało utworzone. <br /> Na twoją pocztę został wysłany email z linkiem aktywacyjnym, który będzie ważny 7 dni."> </asp:Label>

<br />
<br />

<asp:HyperLink ID="Register_Return" runat="server" Text="Powrót na stronę główną." NavigateUrl="~/Default.aspx" />

<br />
<br />

<br />
</div>  
</asp:Content>

<asp:Content ID="Content1" runat="server" Visible=false contentplaceholderid="left_content" >

</asp:content>
