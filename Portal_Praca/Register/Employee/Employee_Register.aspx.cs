﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data.SqlClient;
using System.Text;
using System.Text.RegularExpressions;
using System.Data;
using System.Web.Security;
using System.Security.Cryptography;

namespace Potal_Praca.Register.Employee
{
    public partial class Employee_Register : System.Web.UI.Page
    {
        //parametry do bazy danych
        SqlConnection con;

        //tabele do wartosci z bazy
        string[] table_login;
        string[] table_employee;

        protected void Page_Load(object sender, EventArgs e)
        {

            //Response.Write("<script language=\"javascript\"> alert('" + DateTime.Now.ToString() + "'); </script>");

            con = new SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings["Portal_pracaConnectionString"].ConnectionString);

        }

        protected void Register(object sender, System.EventArgs e)
        {

            try
            {
                this.Validator(sender, e);

                table_login = new String[7];
                table_employee = new String[6];
                String tmp;

                table_login[0] = Employee_Login.Text.Trim(); //login
                table_login[1] = Employee_Password.Text; // hasło
                table_login[2] = Employee_Email.Text; //email
                table_login[3] = "Employee"; //typ tabeli
                table_login[4] = this.employee_id(sender, e); //id typy tabeli
                table_login[5] = "false"; //aktywne
                table_login[6] = Guid.NewGuid().ToString(); //token

                tmp = Employee_Name.Text;
                table_employee[0] = Employee_Name.Text; //imie lub imiona
                table_employee[1] = Employee_Surname.Text; // nazwisko
                table_employee[2] = Employee_City.Text.Trim(); // miasto
                table_employee[3] = Employee_Adress.Text.Trim(); // adres 
                table_employee[4] = Employee_PESEL.Text.Trim(); //PESEL
                table_employee[5] = this.logins_id(sender, e); // id z tabeli logins

                this.Validator_SQL(sender, e, table_login[0], table_login[2], table_employee[4]);

                this.SQL_Execiute(sender, e);

                String login_id = this.logins_id(sender, e);

                Response.Redirect("../Register_Final.aspx?login=" + table_login[0]);


            }
            catch (Exception a)
            {
                Error_Label.Visible = true;
                Error_Label.ForeColor = System.Drawing.ColorTranslator.FromHtml("red");
                Error_Label.Text = a.Message;

            }

        }

        protected void Validator(object sender, System.EventArgs e)
        {
            String login, password, password_conform, email, name, surname, address, city, pesel;

            login = Employee_Login.Text.Trim();
            password = Employee_Password.Text;
            password_conform = Employee_Password_Conform.Text;
            email = Employee_Email.Text;
            name = Employee_Name.Text.Trim();
            surname = Employee_Surname.Text.Trim();
            address = Employee_Adress.Text.Trim();
            city = Employee_City.Text.Trim();
            pesel = Employee_PESEL.Text.Trim();

            /* email: 
             * pierwsza część: 0-9, a-z, A-Z oraz _ i .
             * druga część: @ + a-z, A-Z, 0-9 oraz _ i .
             * trzecia część: . + a-z, A-Z {od 2 do 4 znaków}
            */
            String pattern_email = "^[a-zA-Z0-9.\\-_]+@[a-zA-Z0-9\\-.]+\\.[a-zA-Z]{2,4}$";

            /* pesel:
             * cyfry 0-9 {dokładnie 11 cyfr}
            */
            String pattern_pesel = "^[0-9]{11}$";

            /* hasło:
             * znaki: a-z, A-Z, 0-9 {od 8 do 15 znaków}
             */
            String pattern_password = "[a-zA-Z0-9]{8,15}";

            //login
            if (login == "" || login == null)
            {
                Employee_Login_Label.ForeColor = System.Drawing.ColorTranslator.FromHtml("red");
                throw new Exception("Nie podałeś loginu");
            }
            else
            {
                Employee_Login_Label.ForeColor = System.Drawing.ColorTranslator.FromHtml("gray"); //gray ?
                Error_Label.Visible = false;
            }

            //haslo i jego potwierdzenie
            if (password == "" || password == null || password_conform == "" || password_conform == null)
            {
                Employee_Password_Label.ForeColor = System.Drawing.ColorTranslator.FromHtml("red");
                Employee_Password_Conform_Label.ForeColor = System.Drawing.ColorTranslator.FromHtml("red");
                throw new Exception("Nie podano hasła lub jego potwierdzenia");
            }
            else if (Regex.IsMatch(password_conform, pattern_password) == false || Regex.IsMatch(password, pattern_password) == false)
            {
                Employee_Password_Label.ForeColor = System.Drawing.ColorTranslator.FromHtml("red");
                Employee_Password_Conform_Label.ForeColor = System.Drawing.ColorTranslator.FromHtml("red");
                Employee_Password_Info.ForeColor = System.Drawing.ColorTranslator.FromHtml("red");
                throw new Exception("Wprowadziłeś zły format hasła");
            }
            else
            {
                Employee_Password_Label.ForeColor = System.Drawing.ColorTranslator.FromHtml("gray"); //gray ?
                Employee_Password_Conform_Label.ForeColor = System.Drawing.ColorTranslator.FromHtml("gray"); //gray ?
                Employee_Password_Info.ForeColor = System.Drawing.ColorTranslator.FromHtml("gray");
                Error_Label.Visible = false;
            }

            //email
            if (email == "" || email == null)
            {
                Employee_Email_Label.ForeColor = System.Drawing.ColorTranslator.FromHtml("red");
                throw new Exception("Nie podano adresu email");
            }
            else if (Regex.IsMatch(email, pattern_email) == false)
            {
                Employee_Email_Label.ForeColor = System.Drawing.ColorTranslator.FromHtml("red");
                throw new Exception("Podany email jest nieprawidłowy");
            }
            else
            {
                Employee_Email_Label.ForeColor = System.Drawing.ColorTranslator.FromHtml("gray"); //gray ?
                Error_Label.Visible = false;
            }

            //nazwa firmy
            if (name == "" || name == null)
            {
                Employee_Name_Label.ForeColor = System.Drawing.ColorTranslator.FromHtml("red");
                throw new Exception("Nie podano imienia (imion)");
            }
            else
            {
                Employee_Name_Label.ForeColor = System.Drawing.ColorTranslator.FromHtml("gray"); //gray ?
                Error_Label.Visible = false;
            }

            //adres firmy
            if (address == "" || address == null)
            {
                Employee_Adress_Label.ForeColor = System.Drawing.ColorTranslator.FromHtml("red");
                throw new Exception("Nie podano adresu");
            }
            else
            {
                Employee_Adress_Label.ForeColor = System.Drawing.ColorTranslator.FromHtml("gray"); //gray ?
                Error_Label.Visible = false;
            }

            //REGON
            if (pesel == "" || pesel == null)
            {
                Employee_PESEL_Label.ForeColor = System.Drawing.ColorTranslator.FromHtml("red");
                throw new Exception("Nie podano PESEL-u");
            }
            else if (Regex.IsMatch(pesel, pattern_pesel) == false)
            {
                Employee_PESEL_Label.ForeColor = System.Drawing.ColorTranslator.FromHtml("red");
                throw new Exception("Wprowadzony PESEL jest nieprawidłowy");
            }
            else
            {
                Employee_PESEL_Label.ForeColor = System.Drawing.ColorTranslator.FromHtml("gray"); //gray ?
                Error_Label.Visible = false;
            }

            //czy haslo i jego potweirdzenie sa takie same
            if (password.Equals(password_conform) == false)
            {
                Employee_Password_Label.ForeColor = System.Drawing.ColorTranslator.FromHtml("red");
                Employee_Password_Conform_Label.ForeColor = System.Drawing.ColorTranslator.FromHtml("red");
                throw new Exception("Podane hasła różnią się od siebie");
            }
            else
            {
                Employee_Password_Label.ForeColor = System.Drawing.ColorTranslator.FromHtml("gray");
                Employee_Password_Conform_Label.ForeColor = System.Drawing.ColorTranslator.FromHtml("gray");
                Error_Label.Visible = false;
            }

            //zapoznanie sie z regulaminem
            if (Employee_Regulations_Check.Checked == false)
            {
                Employee_Regulations_Label.ForeColor = System.Drawing.ColorTranslator.FromHtml("red");
                throw new Exception("Nie zapoznałeś się z regulaminem");
            }
            else
            {
                Employee_Regulations_Label.ForeColor = System.Drawing.ColorTranslator.FromHtml("gray");
                Error_Label.Visible = false;
            }


        }

        protected void SQL_Execiute(object sender, System.EventArgs e)
        {
            SqlCommand command_logins = new SqlCommand("INSERT INTO Logins (Login, Password, Email, Type, Id_Table, Active, Register_Date, Token ) "
                                                        + "VALUES (@Employee_Login, @Employee_Password, @Employee_Email, @Type, @Id_table, @Active, @Date, @Token)", con);

            SqlCommand command_employees = new SqlCommand("INSERT INTO Employees (Name, Surname, Address, City, PESEL, Id_Logins)"
                                                            + "VALUES (@Employee_Name, @Employee_Surname, @Employee_Address, @Employee_City, @Employee_PESEL, @Id_Login)", con);

            con.Open();

            //logins
            command_logins.Parameters.Add("@Employee_Login", SqlDbType.NVarChar, 20);
            command_logins.Parameters.Add("@Employee_Password", SqlDbType.NVarChar, 50);
            command_logins.Parameters.Add("@Employee_Email", SqlDbType.NVarChar, 40);
            command_logins.Parameters.Add("@Type", SqlDbType.NVarChar, 20);
            command_logins.Parameters.Add("@Id_Table", SqlDbType.Int);
            command_logins.Parameters.Add("@Active", SqlDbType.Bit);
            command_logins.Parameters.Add("@Date", SqlDbType.DateTime);
            command_logins.Parameters.Add("@Token", SqlDbType.Text);

            command_logins.Parameters["@Employee_Login"].Value = table_login[0];
            command_logins.Parameters["@Employee_Password"].Value = FormsAuthentication.HashPasswordForStoringInConfigFile(table_login[1], "sha1");
            command_logins.Parameters["@Employee_Email"].Value = table_login[2];
            command_logins.Parameters["@Type"].Value = table_login[3];
            command_logins.Parameters["@Id_table"].Value = Int32.Parse(table_login[4]);
            command_logins.Parameters["@Active"].Value = bool.Parse(table_login[5]);
            command_logins.Parameters["@Date"].Value = DateTime.Now;
            command_logins.Parameters["@Token"].Value = table_login[6];

            command_logins.ExecuteNonQuery();

            //Employees
            command_employees.Parameters.Add("@Employee_Name", SqlDbType.NVarChar, 40);
            command_employees.Parameters.Add("@Employee_Surname", SqlDbType.NVarChar, 30);
            command_employees.Parameters.Add("@Employee_Address", SqlDbType.NVarChar, 40);
            command_employees.Parameters.Add("@Employee_City", SqlDbType.NVarChar, 40);
            command_employees.Parameters.Add("@Employee_PESEL", SqlDbType.NVarChar, 11);
            command_employees.Parameters.Add("@Id_Login", SqlDbType.Int);

            command_employees.Parameters["@Employee_Name"].Value = table_employee[0];
            command_employees.Parameters["@Employee_Surname"].Value = table_employee[1];
            command_employees.Parameters["@Employee_Address"].Value = table_employee[3];
            command_employees.Parameters["@Employee_City"].Value = table_employee[2];
            command_employees.Parameters["@Employee_PESEL"].Value = table_employee[4];
            command_employees.Parameters["@Id_Login"].Value = table_employee[5];

            command_employees.ExecuteNonQuery();

            con.Close();

        }

        protected String employee_id(object sender, EventArgs e)
        {
            SqlCommand c = new SqlCommand("SELECT TOP 1 * FROM Employees ORDER BY ID DESC", con);
            String employee_id;
            int tmp;

            con.Open();
            //Response.Write("<script language=\"javascript\"> alert('" + c.ExecuteScalar() + "'); </script>");

            if (c.ExecuteScalar() == null)
            {
                tmp = 1;
                employee_id = tmp.ToString();
            }
            else
            {
                tmp = int.Parse(c.ExecuteScalar().ToString());
                tmp++;
                employee_id = tmp.ToString();
            }

            con.Close();

            return employee_id;
        }

        protected String logins_id(object sender, EventArgs e)
        {
            SqlCommand c = new SqlCommand("SELECT TOP 1 * FROM Logins ORDER BY ID DESC", con);
            String login_id;
            int tmp;

            con.Open();
            //Response.Write("<script language=\"javascript\"> alert('" + c.ExecuteScalar() + "'); </script>");

            if (c.ExecuteScalar() == null)
            {
                tmp = 1;
                login_id = tmp.ToString();
            }
            else
            {
                tmp = int.Parse(c.ExecuteScalar().ToString());
                tmp++;
                login_id = tmp.ToString();
            }

            con.Close();

            return login_id;
        }


        protected void Validator_SQL(object sender, System.EventArgs e, String login, String email, String regon)
        {

            SqlCommand c1 = new SqlCommand("SELECT Login, Email FROM Logins", con);
            SqlCommand c2 = new SqlCommand("SELECT PESEL FROM Employees", con);

            con.Open();
            SqlDataReader sdr1 = c1.ExecuteReader();

            while (sdr1.Read())
            {
                if (sdr1.GetValue(0).ToString() == login)
                {
                    throw new Exception("Podany Login istnieje już w bazie.");
                }
                if (sdr1.GetValue(1).ToString() == email)
                {
                    throw new Exception("Podany Email istnieje już w bazie.");
                }

            }
            sdr1.Close();

            sdr1 = c2.ExecuteReader();

            while (sdr1.Read())
            {
                if (sdr1.GetValue(0).ToString() == regon)
                {
                    throw new Exception("Użytkownik z tym numerem PESEL istnieje w naszej bazie.");
                }
            }
            sdr1.Close();

            con.Close();

        }


    }
}