﻿<%@ Page Title="Rejestracja Osoby Fizycznej" Language="C#" MasterPageFile="~/Site.Master" 
AutoEventWireup="true" CodeBehind="Employee_Register.aspx.cs" Inherits="Potal_Praca.Register.Employee.Employee_Register" %>

<asp:Content ID="HeaderContent" runat="server" ContentPlaceHolderID="HeadContent">
    <style type="text/css">
        #Employer_Login
        {
            width: 230px;
        }
        #Employer_Password
        {
            width: 230px;
        }
        #Epmloyer_Email
        {
            width: 230px;
        }
        #Employer_Company_Name
        {
            width: 230px;
        }
        #Employer_Company_Adress
        {
            width: 230px;
        }
        #Employer_CEO_Name
        {
            width: 230px;
        }
        #Employer_REGON
        {
            width: 230px;
        }
        
        .style11
        {
            height: 30px;
            width: auto;
        }
       
        .style12
        {
            width: 320px;
        }
       
    </style>
</asp:Content>

<asp:Content ID="Body" runat="server" contentplaceholderid="MainContent" >
    <table align="left" cellspacing="15px">
        <tr>
            <td colspan="2">
                <asp:Label ID="Info" runat="server" Text="Punkty oznaczone gwiazdką (*) są obowiązkowe do wypełnienia"> </asp:Label>
            </td>
            </tr>
            <tr>
            <td colspan="2" style="color: Red; ">
                <asp:Label ID="Error_Label" runat="server" Visible="false"> </asp:Label>
            </td>
        </tr>
        
        <tr>
            <td class="style12" >
                <asp:Label ID="Employee_Login_Label" runat="server" Text="Login:*"></asp:Label>
                </td>
                <td class="style11">
                    <asp:TextBox ID="Employee_Login" runat="server" Width="200px"> </asp:TextBox>
                </td>
        </tr>
        <tr>
            <td class="style12" >
                <asp:Label ID="Employee_Password_Label" runat="server" Text="Hasło:*"></asp:Label>
            </td>
            <td class="style9">
                <asp:TextBox ID="Employee_Password" runat="server" TextMode="Password" Width="200px"> </asp:TextBox>
            </td>

            <td>
                <asp:Label ID="Employee_Password_Info" runat="server" Text="Hasło powinno zawierać od 8 do 15 znaków."></asp:Label>
            </td>

        </tr>
        <tr>
            <td class="style12" >
                <asp:Label ID="Employee_Password_Conform_Label" runat="server" Text="Powtórz Hasło:*"></asp:Label>
            </td>
            <td class="style9">
                <asp:TextBox ID="Employee_Password_Conform" runat="server" TextMode="Password" Width="200px"> </asp:TextBox>
            </td>
        </tr>

        <tr>
            <td class="style12" >
                <asp:Label ID="Employee_Email_Label" runat="server" Text="Email:*"></asp:Label>
            </td>
            <td class="style11" >
                <asp:TextBox ID="Employee_Email" runat="server" Width="200px"> </asp:TextBox>
            </td>
        </tr>
        
        <tr>
            <td class="style12" >
                <asp:Label ID="Employee_Name_Label" runat="server" Text="Imie (Imiona):*"></asp:Label>
            </td>

            <td class="style9">
                <asp:TextBox ID="Employee_Name" runat="server" Width="200px"> </asp:TextBox>
            </td>

            <td class="style12" >
                <asp:Label ID="Label1" runat="server" Text="Jeśli użytkownik posiada więcej niż jedno imię, proszę wpisać je w tym polu."></asp:Label>
            </td>
        </tr>
        
        <tr>
            <td class="style12">
                <asp:Label ID="Employee_Surname_Label" runat="server" Text="Nazwisko:*"></asp:Label>
            </td>
            <td class="style9">
                <asp:TextBox ID="Employee_Surname" runat="server" Width="200px"> </asp:TextBox>
            </td>
        </tr>

        <tr>
            <td class="style12">
                <asp:Label ID="Employee_Adress_Label" runat="server" Text="Adres:*"></asp:Label>
            </td>
            <td class="style9">
                <asp:TextBox ID="Employee_Adress" runat="server" Width="200px"> </asp:TextBox>
            </td>
        </tr>
        
        <tr>
            <td class="style12">
                <asp:Label ID="Employee_City_Label" runat="server" Text="Miasto:*"></asp:Label>
            </td>
            <td class="style9">                
                <asp:TextBox ID="Employee_City" runat="server" Width="200px" Enabled="true" > </asp:TextBox>
            </td>

        </tr>
        
        <tr>
            <td class="style12" >
                <asp:Label ID="Employee_PESEL_Label" runat="server" Text="PESEL:*"></asp:Label>
            </td>
            <td class="style9">
                <asp:TextBox ID="Employee_PESEL" runat="server" Width="200px"> </asp:TextBox>
            </td>
        </tr>

        <tr>
            <td class="style12" >
                <asp:Label ID="Employee_Regulations_Label" runat="server" Text="Oświadczam, że zapoznałem/am się z <a href=Employer_Regulations.aspx >Regulaminem</a>* "> </asp:Label> 
            </td>
            <td class="style9">
                <asp:CheckBox id="Employee_Regulations_Check" runat="server" />
            </td>
        </tr>

         <tr>
            <td> 

            </td>    
            <td id="Register_Button" align="right" style="margin-right: 30px;"> 
                <asp:Button ID="Employee_Register_Button" runat="server" Text="Rejestruj" OnClick="Register">  </asp:Button>
            </td>    
        </tr>
    </table>   

</asp:Content>
