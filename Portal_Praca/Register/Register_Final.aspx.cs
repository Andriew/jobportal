﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Net.Mail;
using System.Data.SqlClient;

namespace Potal_Praca.Register
{
    public partial class Register_Final : System.Web.UI.Page
    {
        SqlConnection con;

        protected void Page_Load(object sender, EventArgs e)
        {

            if (Request["login"] == null)
            {
                Response.Redirect("~/Default.aspx");
            }

            String email = "", token = "", login = Request["login"], content;

            con = new SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings["Portal_pracaConnectionString"].ConnectionString);

            SqlCommand command = new SqlCommand("Select Email, Token FROM Logins WHERE Login LIKE '" + login + "'", con);


            con.Open();
            SqlDataReader sdr = command.ExecuteReader();

            while (sdr.Read())
            {
                email = sdr.GetValue(0).ToString();
                token = sdr.GetValue(1).ToString();
            }
            sdr.Close();
            con.Close();

            content = "Witaj! <b>" + login + "</b><br /><br /> Zarejestrowałeś się w PortalPraca.<br />" +
                      "Aby aktywować swoje konto kliknij na poniższy link: <br />" +
                      "http://portalpraca.dyndns.org:90/Default.aspx?option=activate&token=" + token + "<br /> <br />" +
                      "Pozdrawiam <br /> Administrator Portal Praca <br /> <br />" +
                      "Ta wiadomość została wygenerowana automatycznie, prosimi na nią nie odpowiadać.";

            MailMessage mm = new MailMessage("admin@portalpraca.dyndns.org", email, "email aktywacyjny - portal praca", content);
            mm.IsBodyHtml = true;
            SmtpClient sc = new SmtpClient("127.0.0.1", 25);
            //SmtpClient sc = new SmtpClient("37.131.160.18", 25);
            sc.Credentials = new System.Net.NetworkCredential("admin@portalpraca.dyndns.org", "12345");

            //sc.Send(mm);
            mm.Dispose();
        }
    }
}