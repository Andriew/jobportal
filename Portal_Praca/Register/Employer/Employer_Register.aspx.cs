﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data.SqlClient;
using System.Text;
using System.Text.RegularExpressions;
using System.Data;
using System.Web.Security;
using System.Security.Cryptography;

namespace Potal_Praca.Register.Employer
{
    public partial class Employer_Register : System.Web.UI.Page
    {
        //parametry do bazy danych
        SqlConnection con;

        //tabele do wartosci z bazy
        string[] table_login;
        string[] table_employer;

        protected void Page_Load(object sender, EventArgs e)
        {
            //Response.Write("<script language=\"javascript\"> alert('" + DateTime.Now.ToString() + "'); </script>");

            con = new SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings["Portal_pracaConnectionString"].ConnectionString);

        }

        protected void Register(object sender, System.EventArgs e)
        {

            try
            {
                this.Validator(sender, e);

                table_login = new String[7];
                table_employer = new String[6];

                table_login[0] = Employer_Login.Text.Trim(); //login
                table_login[1] = Employer_Password.Text; // hasło
                table_login[2] = Employer_Email.Text; //email
                table_login[3] = "Employer"; //typ tabeli
                table_login[4] = this.employer_id(sender, e); //id typy tabeli
                table_login[5] = "false"; //aktywne
                table_login[6] = Guid.NewGuid().ToString(); //token

                table_employer[0] = Employer_Company_Name.Text.Trim(); // nazwa firmy
                table_employer[1] = Employer_Company_Adress.Text.Trim(); // adres firmy
                table_employer[2] = Employer_Company_City.Text.Trim(); //miasto
                table_employer[3] = Employer_CEO_Name.Text.Trim(); //imie i nazwisko właściciela (może być puste)
                table_employer[4] = Employer_REGON.Text.Trim(); // regon firmy lub spółki
                table_employer[5] = this.logins_id(sender,e); // regon firmy lub spółki

                this.Validator_SQL(sender, e, table_login[0], table_login[2], table_employer[3]);

                this.SQL_Execiute(sender, e);

                String login_id = this.logins_id(sender, e);

                Response.Redirect("../Register_Final.aspx?login=" + table_login[0]);


            }
            catch (Exception a)
            {
                Error_Label.Visible = true;
                Error_Label.ForeColor = System.Drawing.ColorTranslator.FromHtml("red");
                Error_Label.Text = a.Message;

            }

        }



        protected void Validator(object sender, System.EventArgs e)
        {
            String login, password, password_conform, email, company_name, company_address, CEO, regon, city;

            login = Employer_Login.Text.Trim();
            password = Employer_Password.Text;
            password_conform = Employer_Password_Conform.Text;
            email = Employer_Email.Text;
            company_name = Employer_Company_Name.Text.Trim();
            company_address = Employer_Company_Adress.Text.Trim();
            CEO = Employer_CEO_Name.Text.Trim();
            regon = Employer_REGON.Text.Trim();
            city = Employer_Company_City.Text.Trim();

            /* email: 
             * pierwsza część: 0-9, a-z, A-Z oraz _ i .
             * druga część: @ + a-z, A-Z, 0-9 oraz _ i .
             * trzecia część: . + a-z, A-Z {od 2 do 4 znaków}
            */
            String pattern_email = "^[a-zA-Z0-9.\\-_]+@[a-zA-Z0-9\\-.]+\\.[a-zA-Z]{2,4}$";

            /* regon:
             * cyfry 0-9 {dokładnie 9 cyfr}
            */
            String pattern_regon = "^[0-9]{9}$";

            /* hasło:
             * znaki: a-z, A-Z, 0-9 {od 8 do 15 znaków}
             */
            String pattern_password = "[a-zA-Z0-9]{8,15}";

            //login
            if (login == "" || login == null)
            {
                Employer_Login_Label.ForeColor = System.Drawing.ColorTranslator.FromHtml("red");
                throw new Exception("Nie podałeś loginu");
            }
            else
            {
                Employer_Login_Label.ForeColor = System.Drawing.ColorTranslator.FromHtml("gray"); //gray ?
                Error_Label.Visible = false;
            }

            //haslo i jego potwierdzenie
            if (password == "" || password == null || password_conform == "" || password_conform == null)
            {
                Employer_Password_Label.ForeColor = System.Drawing.ColorTranslator.FromHtml("red");
                Employer_Password_Conform_Label.ForeColor = System.Drawing.ColorTranslator.FromHtml("red");
                throw new Exception("Nie podano hasła lub jego potwierdzenia");
            }
            else if (Regex.IsMatch(password_conform, pattern_password) == false || Regex.IsMatch(password, pattern_password) == false)
            {
                Employer_Password_Label.ForeColor = System.Drawing.ColorTranslator.FromHtml("red");
                Employer_Password_Conform_Label.ForeColor = System.Drawing.ColorTranslator.FromHtml("red");
                Employer_Password_Info.ForeColor = System.Drawing.ColorTranslator.FromHtml("red");
                throw new Exception("Wprowadziłeś zły format hasła");
            }
            else
            {
                Employer_Password_Label.ForeColor = System.Drawing.ColorTranslator.FromHtml("gray"); //gray ?
                Employer_Password_Conform_Label.ForeColor = System.Drawing.ColorTranslator.FromHtml("gray"); //gray ?
                Employer_Password_Info.ForeColor = System.Drawing.ColorTranslator.FromHtml("gray");
                Error_Label.Visible = false;
            }

            //email
            if (email == "" || email == null)
            {
                Employer_Email_Label.ForeColor = System.Drawing.ColorTranslator.FromHtml("red");
                throw new Exception("Nie podano adresu email");
            }
            else if (Regex.IsMatch(email, pattern_email) == false)
            {
                Employer_Email_Label.ForeColor = System.Drawing.ColorTranslator.FromHtml("red");
                throw new Exception("Podany email jest nieprawidłowy");
            }
            else
            {
                Employer_Email_Label.ForeColor = System.Drawing.ColorTranslator.FromHtml("gray"); //gray ?
                Error_Label.Visible = false;
            }

            //nazwa firmy
            if (company_name == "" || company_name == null)
            {
                Employer_Company_Name_Label.ForeColor = System.Drawing.ColorTranslator.FromHtml("red");
                throw new Exception("Nie podano nazwy firmy");
            }
            else
            {
                Employer_Company_Name_Label.ForeColor = System.Drawing.ColorTranslator.FromHtml("gray"); //gray ?
                Error_Label.Visible = false;
            }

            //adres firmy
            if (company_address == "" || company_address == null)
            {
                Employer_Company_Adress_Label.ForeColor = System.Drawing.ColorTranslator.FromHtml("red");
                throw new Exception("Nie podano adresu firmy");
            }
            else
            {
                Employer_Company_Adress_Label.ForeColor = System.Drawing.ColorTranslator.FromHtml("gray"); //gray ?
                Error_Label.Visible = false;
            }

            //miasto firmy
            if (city == "" || city == null)
            {
                Employer_Company_City_Label.ForeColor = System.Drawing.ColorTranslator.FromHtml("red");
                throw new Exception("Nie podano miasta ");
            }
            else
            {
                Employer_Company_City_Label.ForeColor = System.Drawing.ColorTranslator.FromHtml("gray"); //gray ?
                Error_Label.Visible = false;
            }

            //REGON
            if (regon == "" || regon == null)
            {
                Employer_REGON_Label.ForeColor = System.Drawing.ColorTranslator.FromHtml("red");
                throw new Exception("Nie podano regonu firmy");
            }
            else if (Regex.IsMatch(regon, pattern_regon) == false)
            {
                Employer_REGON_Label.ForeColor = System.Drawing.ColorTranslator.FromHtml("red");
                throw new Exception("Wprowadzony regon jest nieprawidłowy");
            }
            else
            {
                Employer_REGON_Label.ForeColor = System.Drawing.ColorTranslator.FromHtml("gray"); //gray ?
                Error_Label.Visible = false;
            }

            //czy haslo i jego potweirdzenie sa takie same
            if (password.Equals(password_conform) == false)
            {
                Employer_Password_Label.ForeColor = System.Drawing.ColorTranslator.FromHtml("red");
                Employer_Password_Conform_Label.ForeColor = System.Drawing.ColorTranslator.FromHtml("red");
                throw new Exception("Podane hasła różnią się od siebie");
            }
            else
            {
                Employer_Password_Label.ForeColor = System.Drawing.ColorTranslator.FromHtml("gray");
                Employer_Password_Conform_Label.ForeColor = System.Drawing.ColorTranslator.FromHtml("gray");
                Error_Label.Visible = false;
            }

            //zapoznanie sie z regulaminem
            if (Employer_Regulations_Check.Checked == false)
            {
                Employer_Regulations_Label.ForeColor = System.Drawing.ColorTranslator.FromHtml("red");
                throw new Exception("Nie zapoznałeś się z regulaminem");
            }
            else
            {
                Employer_Regulations_Label.ForeColor = System.Drawing.ColorTranslator.FromHtml("gray");
                Error_Label.Visible = false;
            }


        }

        protected void SQL_Execiute(object sender, System.EventArgs e)
        {
            SqlCommand command_logins = new SqlCommand("INSERT INTO Logins (Login, Password, Email, Type, Id_Table, Active, Register_Date, Token ) "
                                                        + "VALUES (@Employer_Login, @Employer_Password, @Employer_Email, @Type, @Id_table, @Active, @Date, @Token)", con);

            SqlCommand command_employers = new SqlCommand("INSERT INTO Employers (Company_Name, Company_Address, CEO_Name, REGON, Id_Logins, City)"
                                                            + "VALUES (@Employer_Company_Name, @Employer_Company_Address, @Employer_CEO_Name, @Employer_REGON, @Id_Login, @City)", con);

            con.Open();

            //logins
            command_logins.Parameters.Add("@Employer_Login", SqlDbType.NVarChar, 20);
            command_logins.Parameters.Add("@Employer_Password", SqlDbType.NVarChar, 50);
            command_logins.Parameters.Add("@Employer_Email", SqlDbType.NVarChar, 40);
            command_logins.Parameters.Add("@Type", SqlDbType.NVarChar, 20);
            command_logins.Parameters.Add("@Id_Table", SqlDbType.Int);
            command_logins.Parameters.Add("@Active", SqlDbType.Bit);
            command_logins.Parameters.Add("@Date", SqlDbType.DateTime);
            command_logins.Parameters.Add("@Token", SqlDbType.Text);

            command_logins.Parameters["@Employer_Login"].Value = table_login[0];
            command_logins.Parameters["@Employer_Password"].Value = FormsAuthentication.HashPasswordForStoringInConfigFile(table_login[1], "sha1");
            command_logins.Parameters["@Employer_Email"].Value = table_login[2];
            command_logins.Parameters["@Type"].Value = table_login[3];
            command_logins.Parameters["@Id_table"].Value = Int32.Parse(table_login[4]);
            command_logins.Parameters["@Active"].Value = bool.Parse(table_login[5]);
            command_logins.Parameters["@Date"].Value = DateTime.Now;
            command_logins.Parameters["@Token"].Value = table_login[6];

            command_logins.ExecuteNonQuery();

            //Employers
            command_employers.Parameters.Add("@Employer_Company_Name", SqlDbType.NVarChar, 40);
            command_employers.Parameters.Add("@Employer_Company_Address", SqlDbType.NVarChar, 50);
            command_employers.Parameters.Add("@Employer_CEO_Name", SqlDbType.NVarChar, 40);
            command_employers.Parameters.Add("@Employer_REGON", SqlDbType.NVarChar, 9);
            command_employers.Parameters.Add("@Id_Login", SqlDbType.Int);
            command_employers.Parameters.Add("@City", SqlDbType.NVarChar, 30);

            command_employers.Parameters["@Employer_Company_Name"].Value = table_employer[0];
            command_employers.Parameters["@Employer_Company_Address"].Value = table_employer[1];
            command_employers.Parameters["@City"].Value = table_employer[2];
            command_employers.Parameters["@Employer_CEO_Name"].Value = table_employer[3];
            command_employers.Parameters["@Employer_REGON"].Value = table_employer[4];
            command_employers.Parameters["@Id_Login"].Value = table_employer[5];
            

            command_employers.ExecuteNonQuery();

            con.Close();

        }

        protected String employer_id(object sender, EventArgs e)
        {
            SqlCommand c = new SqlCommand("SELECT TOP 1 * FROM Employers ORDER BY ID DESC", con);
            String employer_id;
            int tmp;

            con.Open();
            //Response.Write("<script language=\"javascript\"> alert('" + c.ExecuteScalar() + "'); </script>");

            if (c.ExecuteScalar() == null)
            {
                tmp = 1;
                employer_id = tmp.ToString();
            }
            else
            {
                tmp = int.Parse(c.ExecuteScalar().ToString());
                tmp++;
                employer_id = tmp.ToString();
            }

            con.Close();

            return employer_id;
        }

        protected String logins_id(object sender, EventArgs e)
        {
            SqlCommand c = new SqlCommand("SELECT TOP 1 * FROM Logins ORDER BY ID DESC", con);
            String login_id;
            int tmp;

            con.Open();
            //Response.Write("<script language=\"javascript\"> alert('" + c.ExecuteScalar() + "'); </script>");

            if (c.ExecuteScalar() == null)
            {
                tmp = 1;
                login_id = tmp.ToString();
            }
            else
            {
                tmp = int.Parse(c.ExecuteScalar().ToString());
                tmp++;
                login_id = tmp.ToString();
            }

            con.Close();

            return login_id;
        }

        protected void Hide_CEO(object sender, System.EventArgs e)
        {
            if (Company_Check.Checked == false)
            {
                Employer_CEO_Name.Enabled = true;
            }

            if (Company_Check.Checked == true)
            {
                Employer_CEO_Name.Enabled = false;
            }

        }

        protected void Validator_SQL(object sender, System.EventArgs e, String login, String email, String regon)
        {


            SqlCommand c1 = new SqlCommand("SELECT Login, Email FROM Logins", con);
            SqlCommand c2 = new SqlCommand("SELECT REGON FROM Employers", con);

            con.Open();
            SqlDataReader sdr1 = c1.ExecuteReader();

            while (sdr1.Read())
            {
                if (sdr1.GetValue(0).ToString() == login)
                {
                    throw new Exception("Podany Login istnieje już w bazie.");
                }
                if (sdr1.GetValue(1).ToString() == email)
                {
                    throw new Exception("Podany Email istnieje już w bazie.");
                }

            }
            sdr1.Close();

            sdr1 = c2.ExecuteReader();

            while (sdr1.Read())
            {
                if (sdr1.GetValue(0).ToString() == regon)
                {
                    throw new Exception("Firma lub Spółka o tym regonie istnieje już w bazie.");
                }
            }
            sdr1.Close();

            con.Close();

        }


    }
}