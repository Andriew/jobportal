﻿<%@ Page Title="Rejestracja Firmy" Language="C#" AutoEventWireup="true" 
CodeBehind="Employer_Register.aspx.cs"  MasterPageFile="~/Site.master" Inherits="Potal_Praca.Register.Employer.Employer_Register" %>

<asp:Content ID="HeaderContent" runat="server" ContentPlaceHolderID="HeadContent">
    <style type="text/css">
        #Employer_Login
        {
            width: 230px;
        }
        #Employer_Password
        {
            width: 230px;
        }
        #Epmloyer_Email
        {
            width: 230px;
        }
        #Employer_Company_Name
        {
            width: 230px;
        }
        #Employer_Company_Adress
        {
            width: 230px;
        }
        #Employer_CEO_Name
        {
            width: 230px;
        }
        #Employer_REGON
        {
            width: 230px;
        }
        
        .style11
        {
            height: 30px;
            width: auto;
        }
       
        .style12
        {
            width: 320px;
        }
       
    </style>
</asp:Content>

<asp:Content ID="Body" runat="server" contentplaceholderid="MainContent" >
    <table align="left" cellspacing="15px">
        <tr>
            <td colspan="2">
                <asp:Label ID="Info" runat="server" Text="Punkty oznaczone gwiazdką (*) są obowiązkowe do wypełnienia"> </asp:Label>
            </td>
            </tr>
            <tr>
            <td colspan="2" style="color: Red; ">
                <asp:Label ID="Error_Label" runat="server" Visible="false"> </asp:Label>
            </td>
        </tr>
        
        <tr>
            <td class="style12" >
                <asp:Label ID="Employer_Login_Label" runat="server" Text="Login:*"></asp:Label>
                </td>
                <td class="style11">
                    <asp:TextBox ID="Employer_Login" runat="server" Width="200px"> </asp:TextBox>
                </td>
        </tr>
        <tr>
            <td class="style12" >
                <asp:Label ID="Employer_Password_Label" runat="server" Text="Hasło:*"></asp:Label>
            </td>
            <td class="style9">
                <asp:TextBox ID="Employer_Password" runat="server" TextMode="Password" Width="200px"> </asp:TextBox>
            </td>

            <td>
                <asp:Label ID="Employer_Password_Info" runat="server" Text="Hasło powinno zawierać od 8 do 15 znaków."></asp:Label>
            </td>

        </tr>
        <tr>
            <td class="style12" >
                <asp:Label ID="Employer_Password_Conform_Label" runat="server" Text="Powtórz Hasło:*"></asp:Label>
            </td>
            <td class="style9">
                <asp:TextBox ID="Employer_Password_Conform" runat="server" TextMode="Password" Width="200px"> </asp:TextBox>
            </td>
        </tr>

        <tr>
            <td class="style12" >
                <asp:Label ID="Employer_Email_Label" runat="server" Text="Email:*"></asp:Label>
            </td>
            <td class="style11" >
                <asp:TextBox ID="Employer_Email" runat="server" Width="200px"> </asp:TextBox>
            </td>
        </tr>
        
        <tr>
            <td class="style12" >
                <asp:Label ID="Employer_Company_Name_Label" runat="server" Text="Skrócona Nazwa Firmy:*"></asp:Label>
            </td>
            <td class="style9">
                <asp:TextBox ID="Employer_Company_Name" runat="server" Width="200px"> </asp:TextBox>
            </td>
        </tr>
        
        <tr>
            <td class="style12">
                <asp:Label ID="Employer_Company_Adress_Label" runat="server" Text="Adres Firmy:*"></asp:Label>
            </td>
            <td class="style9">
                <asp:TextBox ID="Employer_Company_Adress" runat="server" Width="200px"> </asp:TextBox>
            </td>
        </tr>
         <tr>
            <td class="style12">
                <asp:Label ID="Employer_Company_City_Label" runat="server" Text="Miasto:*"></asp:Label>
            </td>
            <td class="style9">
                <asp:TextBox ID="Employer_Company_City" runat="server" Width="200px"> </asp:TextBox>
            </td>
        </tr>
        <tr>
            <td class="style12">
                <asp:Label ID="Employer_CEO_Name_Label" runat="server" Text="Imię i nazwisko właściciela firmy:*"></asp:Label>
            </td>
            <td class="style9">                
                <asp:TextBox ID="Employer_CEO_Name" runat="server" Width="200px" Enabled="true" > </asp:TextBox>
            </td>

            <td class="style11">                
                <asp:CheckBox id="Company_Check" runat="server" AutoPostBack="True" OnCheckedChanged="Hide_CEO" />
                <asp:Label ID="Company_Label" runat="server" Text="Jestem współwłaścicielem spółki lub osbą która reprezentuje spółkę w której nie ma określonego jednego właściciela."> </asp:Label>
            </td>


        </tr>
        
        <tr>
            <td class="style12" >
                <asp:Label ID="Employer_REGON_Label" runat="server" Text="REGON:*"></asp:Label>
            </td>
            <td class="style9">
                <asp:TextBox ID="Employer_REGON" runat="server" Width="200px"> </asp:TextBox>
            </td>
        </tr>

        <tr>
            <td class="style12" >
                <asp:Label ID="Employer_Regulations_Label" runat="server" Text="Oświadczam, że zapoznałem/am się z <a href=Employer_Regulations.aspx >Regulaminem</a>* "> </asp:Label> 
            </td>
            <td class="style9">
                <asp:CheckBox id="Employer_Regulations_Check" runat="server" />
            </td>
        </tr>

         <tr>
            <td> 

            </td>    
            <td id="Register_Button" align="right" style="margin-right: 30px;"> 
                <asp:Button ID="Employer_Register_Button" runat="server" Text="Rejestruj" OnClick="Register">  </asp:Button>
            </td>    
        </tr>
    </table>   
</asp:Content>


