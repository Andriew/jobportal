﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Net.Mail;
using System.Data.SqlClient;
using System.Web.Security;

namespace Portal_Praca.Login
{
    public partial class Forgotten_Password : System.Web.UI.Page
    {
        SqlConnection con;
        protected void Page_Load(object sender, EventArgs e)
        {
            //Response.Write("<script language=\"javascript\"> alert('" + tmp + "'); </script>");
            if (Session["Login"] != null)
            {
                Response.Redirect("~/Account/User_Page.aspx");
            }
            Error_Label.Visible = false;
            con = new SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings["Portal_pracaConnectionString"].ConnectionString);
        }

        protected void send_password(object sender, EventArgs e)
        {
            String login = null, email = null;
            try
            {
                login = Login_text.Text;
                email = Email.Text;

                this.check_DB(sender, e, login, email);
                Response.Write("<script language=\"javascript\"> alert('Na podany adres email zostało wysłane nowe hasło z linkiem akywacyjnym'); </script>");
                Response.Redirect("~/Default.aspx");

            }
            catch (Exception a)
            {
                Error_Label.Visible = true;
                Error_Label.ForeColor = System.Drawing.ColorTranslator.FromHtml("red");
                Error_Label.Text = a.Message;
            }

        }

        protected void check_DB(object sender, EventArgs e, String login, String email)
        {
            String token = "";
            String new_password = "";

            SqlCommand c1 = new SqlCommand("SELECT Login, Email, Token FROM Logins WHERE Login LIKE '" + login + "' AND Email LIKE '" + email + "'", con);
            
            con.Open();
            
            if (c1.ExecuteScalar() == null)
            {
                con.Close();
                throw new Exception("W bazie nie znaleziono użytkownika o tych danych. Sprawdź swoje dane lub się <a href=\"../Register/Register.aspx\"> zarejestruj</a>.");
            }
            else
            {
                SqlDataReader sdr = c1.ExecuteReader();
                while (sdr.Read())
                {
                    token = sdr.GetValue(2).ToString();
                }
                sdr.Close();

                new_password = Guid.NewGuid().ToString().Substring(0, 8);
                this.update_password(sender, e, login, email, new_password);
                this.send_email(sender, e, login, email, token, new_password);
                con.Close();
            }

        }

        protected void send_email(object sender, EventArgs e, String login, String email, String token, String new_password)
        {
            String content;

            content = "Witaj! <b>" + login + "</b><br /><br /> Twoje nowe hasło to: <br /><b>" + new_password +
                      "</b><br /><br /> Ze względów bezpieczeństwa podczas generacji nowego hasła twoje konto zostało zdezaktywowane. <br />"+
                      "Aby ponownie aktywować swoje konto kliknij na poniższy link: <br />" +
                      "http://portalpraca.dyndns.org:90/Default.aspx?option=forgotten_password&token=" + token + "<br /> <br />" +
                      "Pozdrawiam <br /> Administrator Portal Praca <br /> <br />" +
                      "Ta wiadomość została wygenerowana automatycznie, prosimi na nią nie odpowiadać.";

            MailMessage mm = new MailMessage("admin@portalpraca.dyndns.org", email, "przypomnienie hasła - portal praca", content);
            mm.IsBodyHtml = true;
            SmtpClient sc = new SmtpClient("127.0.0.1", 25);
            //SmtpClient sc = new SmtpClient("37.131.160.18", 25);
            sc.Credentials = new System.Net.NetworkCredential("admin@portalpraca.dyndns.org", "12345");

            sc.Send(mm);
            mm.Dispose();
        }

        protected void update_password(object sender, EventArgs e, String login, String email, String new_password)
        {
            new_password = FormsAuthentication.HashPasswordForStoringInConfigFile(new_password, "sha1");
            SqlCommand cmd = new SqlCommand("UPDATE Logins SET Password='"+ new_password+"', Active='False'"+
            "WHERE Login LIKE '"+login+"' AND Email LIKE '"+ email +"'", con);

            cmd.ExecuteNonQuery();
        }
    }
}