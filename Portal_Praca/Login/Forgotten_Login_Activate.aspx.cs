﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data.SqlClient;

namespace Portal_Praca.Login
{
    public partial class Forgotten_Login_Activate : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (Request["token"] == null)
            {
                Response.Redirect("~/Default.aspx");
            }
            else
            {
                SqlConnection con = new SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings["Portal_pracaConnectionString"].ConnectionString);
                con.Open();
                SqlCommand cmd = new SqlCommand("UPDATE Logins SET Active='True' WHERE Token LIKE '" + Request["token"].ToString() + "'", con);
                cmd.ExecuteNonQuery();
                con.Close();
            }
        }
    }
}