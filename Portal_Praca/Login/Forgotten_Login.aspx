﻿<%@ Page Title="Przypomnienie Loginu" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" 
CodeBehind="Forgotten_Login.aspx.cs" Inherits="Portal_Praca.Login.Forgotten_Login" %>

<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server">
<style type="text/css">
     .style11
        {
            height: 30px;
            width: auto;
        }
       
        .style12
        {
            width: 100px;
        }
</style>
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
<table id="table" align="left" cellspacing="15px">
        <tr>
            <td colspan="2" style="color: Red; ">
                <asp:Label ID="Error_Label" runat="server" Visible="false"> </asp:Label>
            </td>
        </tr>
        
        <tr>
            <td class="style12" >
                <asp:Label ID="Email_Label" runat="server" Text="Email:"></asp:Label>
            </td>
            <td class="style13">
                <asp:TextBox ID="Email_text" runat="server" Width="230px"></asp:TextBox>
            </td>
            
        </tr>

        <tr>
            <td class="style12" >
                <asp:Label ID="Conform_Email_Label" runat="server" Text="Potwierdź email:"></asp:Label>
            </td>
            <td class="style13">
                <asp:TextBox ID="Conform_Email" runat="server" Width="230px"></asp:TextBox>
            </td>
        </tr>
        
         <tr>
            <td id="Register_Button" align="right" colspan="2"> 
                <asp:Button ID="Button" runat="server" Text="Wyślij login" 
                    OnClick="send_login" Width="85px">  </asp:Button>
                
            </td>    
        </tr>
</asp:Content>
