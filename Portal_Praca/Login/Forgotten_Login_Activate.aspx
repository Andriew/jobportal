﻿<%@ Page Title="Aktywacja Loginu" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" 
CodeBehind="Forgotten_Login_Activate.aspx.cs" Inherits="Portal_Praca.Login.Forgotten_Login_Activate" %>

<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server">

</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <h1>
        Twoje konto zostało aktywowane. <br />
        Możesz się zalogować używając swojego loginu i hasła. <br />
    </h1>
</asp:Content>
