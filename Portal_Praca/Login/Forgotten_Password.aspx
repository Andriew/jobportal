﻿<%@ Page Title="Przypomnij hasło" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" 
CodeBehind="Forgotten_Password.aspx.cs" Inherits="Portal_Praca.Login.Forgotten_Password" %>

<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server">
<style type="text/css">
     .style11
        {
            height: 30px;
            width: auto;
        }
       
        .style12
        {
            width: 100px;
        }
</style>
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
 
 <table id="table" align="left" cellspacing="15px">
        <tr>
            <td colspan="2" style="color: Red; ">
                <asp:Label ID="Error_Label" runat="server" Visible="false"> </asp:Label>
            </td>
        </tr>
        
        <tr>
            <td class="style12" >
                <asp:Label ID="Login_Label" runat="server" Text="Login:"></asp:Label>
            </td>
            <td class="style13">
                <asp:TextBox ID="Login_text" runat="server" Width="230px"></asp:TextBox>
            </td>
            <td>
                Jeśli zapomniałeś swojego loginu kliknij w <a href="Forgotten_Login.aspx"> ten link </a>
            </td>

        </tr>

        <tr>
            <td class="style12" >
                <asp:Label ID="Email_Label" runat="server" Text="Email:"></asp:Label>
            </td>
            <td class="style13">
                <asp:TextBox ID="Email" runat="server" Width="230px"></asp:TextBox>
            </td>
        </tr>
        
         <tr>
            <td id="Register_Button" align="right" colspan="2"> 
                <asp:Button ID="Button" runat="server" Text="Wyślij hasło" 
                    OnClick="send_password" Width="85px">  </asp:Button>
                
            </td>    
        </tr>
        
</asp:Content>
