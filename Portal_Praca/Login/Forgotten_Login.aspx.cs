﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Net.Mail;
using System.Data.SqlClient;
using System.Web.Security;


namespace Portal_Praca.Login
{
    public partial class Forgotten_Login : System.Web.UI.Page
    {
        SqlConnection con;
        protected void Page_Load(object sender, EventArgs e)
        {
            //Response.Write("<script language=\"javascript\"> alert('" + tmp + "'); </script>");
            if (Session["Login"] != null)
            {
                Response.Redirect("~/Account/User_Page.aspx");
            }
            Error_Label.Visible = false;
            con = new SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings["Portal_pracaConnectionString"].ConnectionString);
        }

        protected void send_login(object sender, EventArgs e)
        {
            String conform_email = null, email = null;
            try
            {
                email = Email_text.Text;
                conform_email = Conform_Email.Text;

                this.check_DB(sender, e, conform_email, email);
                Response.Write("<script language=\"javascript\"> alert('Na podany adres email zostało wysłany twój login z linkiem akywacyjnym'); </script>");
                Response.Redirect("~/Default.aspx");

            }
            catch (Exception a)
            {
                Error_Label.Visible = true;
                Error_Label.ForeColor = System.Drawing.ColorTranslator.FromHtml("red");
                Error_Label.Text = a.Message;
            }

        }

        protected void check_DB(object sender, EventArgs e, String conform_email, String email)
        {
            String token = "";
            String login = "";

            if (email.Equals(conform_email) == false)
            {
                throw new Exception("W bazie nie znaleziono użytkownika o tych danych. Sprawdź dane które wpisałeś lub się <a href=\"../Register/Register.aspx\"> zarejestruj</a>.");
            }

            SqlCommand c1 = new SqlCommand("SELECT Login, Email, Token FROM Logins WHERE Email LIKE '" + email + "'", con);

            con.Open();

            if (c1.ExecuteScalar() == null )
            {
                con.Close();
                throw new Exception("W bazie nie znaleziono użytkownika o tych danych. Sprawdź dane które wpisałeś lub się <a href=\"../Register/Register.aspx\"> zarejestruj</a>.");
            }
            else
            {
                SqlDataReader sdr = c1.ExecuteReader();
                while (sdr.Read())
                {
                    login = sdr.GetValue(0).ToString();
                    //token = sdr.GetValue(1).ToString();
                    token = sdr.GetValue(2).ToString();
                }
                sdr.Close();

                this.update_DB(sender, e, login, email);
                this.send_email(sender, e, login, email, token);
                con.Close();
            }

        }

        protected void send_email(object sender, EventArgs e, String login, String email, String token)
        {
            String content;

            content = "Witaj! <b>" + login + "</b><br /><br /> Twoje Login to: <br /> <b>" + login +
                      "</b><br /><br /> Ze względów bezpieczeństwa podczas opcji przypomnienia loginu twoje konto zostało zdezaktywowane. <br />" +
                      "Aby ponownie aktywować swoje konto kliknij na poniższy link: <br />" +
                      "http://portalpraca.dyndns.org:90/Default.aspx?option=forgotten_login&token=" + token + "<br /> <br />" +
                      "Pozdrawiam <br /> Administrator Portal Praca <br /> <br />" +
                      "Ta wiadomość została wygenerowana automatycznie, prosimi na nią nie odpowiadać.";

            MailMessage mm = new MailMessage("admin@portalpraca.dyndns.org", email, "przypomnienie hasła - portal praca", content);
            mm.IsBodyHtml = true;
            //SmtpClient sc = new SmtpClient("127.0.0.1", 25);
            SmtpClient sc = new SmtpClient("37.131.160.18", 25);
            sc.Credentials = new System.Net.NetworkCredential("admin@portalpraca.dyndns.org", "12345");

            sc.Send(mm);
            mm.Dispose();
        }

        protected void update_DB(object sender, EventArgs e, String login, String email)
        {
            SqlCommand cmd = new SqlCommand("UPDATE Logins SET Active='False'" +
            "WHERE Login LIKE '" + login + "' AND Email LIKE '" + email + "'", con);

            cmd.ExecuteNonQuery();
        }
    }
}