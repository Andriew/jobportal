﻿<%@ Page Title="Aktywacja hasła" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" 
CodeBehind="Forgotten_Password_Activate.aspx.cs" Inherits="Portal_Praca.Login.Forgotten_Password_Activate" %>

<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server">

</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <h1>
        Twoje hasło zostało zmienione. <br />
        Możesz się zalogować używając nowego hasła. <br />
    </h1>
</asp:Content>
