﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.Security;
using System.Security.Cryptography;
using System.Text;
using System.Data.SqlClient;

namespace Potal_Praca.Login
{
    public partial class Login : System.Web.UI.Page
    {
        SqlConnection con;

        protected void Page_Load(object sender, EventArgs e)
        {
            //Response.Write("<script language=\"javascript\"> alert('" + tmp + "'); </script>");
            if (Session["Login"] != null)
            {
                Response.Redirect("~/Account/User_Page.aspx");
            }
            con = new SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings["Portal_pracaConnectionString"].ConnectionString);
        }

        protected void user_check(object sender, EventArgs e)
        {
            String login = null, password = null;
            try
            {
                login = Login_text.Text;
                password = FormsAuthentication.HashPasswordForStoringInConfigFile(Password.Text, "sha1");

                this.check_DB(sender, e, login, password);
                
                if(this.CheckMe_Check.Checked == true)
                {
                    this.CheckMe_fun(sender, e, login, password, Session["Token"].ToString(), Session["Type"].ToString() );
                }
                Response.Redirect("~/Account/User_Page.aspx");
            }
            catch (Exception a)
            {
                Error_Label.Visible = true;
                Error_Label.ForeColor = System.Drawing.ColorTranslator.FromHtml("red");
                Error_Label.Text = a.Message;
            }

        }

        protected void check_DB(object sender, EventArgs e, String login, String password)
        {
            SqlCommand c1 = new SqlCommand("SELECT Active, Login, Password, Token, Type FROM Logins WHERE Login LIKE '" + login + "' AND Password LIKE '" + password + "'", con);

            String tmp= "";
            bool active;
            
            con.Open();
            SqlDataReader sdr2 = c1.ExecuteReader();

            if (sdr2.HasRows == true)
            {
                while (sdr2.Read())
                {

                    if (sdr2.GetValue(0) != null)
                    {
                        tmp = sdr2.GetValue(0).ToString();
                        active = bool.Parse(tmp);

                        if (active == false)
                        {
                            sdr2.Close();
                            con.Close();
                            throw new Exception("Twoje konto nie zostało aktywowane, sprawdź pocztę email.");
                        }
                        else
                        {
                            Session["Login"] = login;
                            Session["Password"] = password;
                            Session["Token"] = sdr2.GetValue(3).ToString();
                            Session["Type"] = sdr2.GetValue(4).ToString();
                            Session["IP"] = Request.ServerVariables["REMOTE_ADDR"];
                        }
                    }
                    else
                    {
                        con.Close();
                        throw new Exception("Podałeś zły login lub hasło.");
                    }
                }
            }
            else
                throw new Exception("Podałeś źly login lub hasło.");

        }

        protected void CheckMe_fun(object sender, EventArgs e, String login, String password, String token, String type)
        {
            HttpCookie myCookie = new HttpCookie("Portal_Praca");
            myCookie.Values.Add("login", login);
            myCookie.Values.Add("password", password);
            myCookie.Values.Add("token", token);
            myCookie.Values.Add("type", type);
            myCookie.Values.Add("IP", Request.ServerVariables["REMOTE_ADDR"]);

            myCookie.Expires = DateTime.Now.AddHours(12);
            Response.Cookies.Add(myCookie);
            
        }
    }
}
