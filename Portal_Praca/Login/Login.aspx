﻿<%@ Page Title="Logowanie" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" 
CodeBehind="Login.aspx.cs" Inherits="Potal_Praca.Login.Login" %>

<asp:Content ID="HeaderContent" runat="server" ContentPlaceHolderID="HeadContent">
    <style type="text/css">
        #Login
        {
            width: 230px;
        }
     
        #table
        {
            height: 30px;
            width: auto;
        }
       
        .style12
        {
            width: 180px;
        }
       
        .style13
        {
            width: 320px;
            height: 30px;
        }
       
    </style>
</asp:Content>

<asp:Content ID="BodyContent" runat="server" ContentPlaceHolderID="MainContent">

    <table id="table" align="left" cellspacing="15px">
        <tr>
            <td colspan="2" style="color: Red; ">
                <asp:Label ID="Error_Label" runat="server" Visible="false"> </asp:Label>
            </td>

        </tr>
        
        <tr>
            <td class="style12" >
                <asp:Label ID="Login_Label" runat="server" Text="Login:"></asp:Label>
            </td>
            <td class="style13">
                <asp:TextBox ID="Login_text" runat="server" Width="230px"></asp:TextBox>
            </td>
            <td>
                Jeśli nie pamiętasz swojego hasła kliknij w <a href="Forgotten_Password.aspx"> ten link </a>
            </td>
            
        </tr>

        <tr>
            <td class="style12" >
                <asp:Label ID="Password_Label" runat="server" Text="Hasło:"></asp:Label>
            </td>
            <td class="style13">
                <asp:TextBox ID="Password" runat="server" TextMode="Password" Width="230px"></asp:TextBox>
            </td>
            <td>
                Jeśli nie pamiętasz swojego loginu kliknij w <a href="Forgotten_Login.aspx"> ten link </a>
            </td>
        </tr>
        
        <tr>
            <td class="style12" >
                <asp:Label ID="CheckMe" runat="server" Text="Zapamiętaj mnie:"> </asp:Label> 
            </td>
            <td>
                <asp:CheckBox id="CheckMe_Check" runat="server" />
            </td>
        </tr>

         <tr>
            <td id="Register_Button" align="right" colspan="2"> 
                <asp:Button ID="Login_Button" runat="server" Text="Zaloguj" OnClick="user_check" Width="73px">  </asp:Button>
                
            </td>    
        </tr>
        
    </table>   
</asp:Content>

