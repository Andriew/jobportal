﻿<%@ Page Title="Zmiana hasła" Language="C#"  MasterPageFile="~/Site.Master"  AutoEventWireup="true" 
CodeBehind="Change_Password.aspx.cs"  Inherits="Portal_Praca.Account.Change_Password" %>

<asp:Content ID="HeaderContent" runat="server" ContentPlaceHolderID="HeadContent">
    <style type="text/css">
        .style11
        {
            height: 30px;
            width: auto;
        }
       
        .style12
        {
            width: 200px;
        }
       
        .style13
        {
            width: 320px;
            height: 51px;
        }
        .style14
        {
            height: 51px;
        }
       
    </style>
</asp:Content>

<asp:Content ID="Body" runat="server" contentplaceholderid="MainContent" >
    <table align="left" cellspacing="15px">
            <tr>
            <td colspan="2" style="color: Red; ">
                <asp:Label ID="Error_Label" runat="server" Visible="false"> </asp:Label>
            </td>
        </tr>
        <tr>
            <td class="style12" >
                <asp:Label ID="Actual_Password_Label" runat="server" Text="Aktualne hasło:"></asp:Label>
                </td>
                <td class="style11">
                    <asp:TextBox ID="Actual_Password" runat="server" Width="200px" TextMode="Password"> </asp:TextBox>
                </td>
        </tr>
        <tr>
            <td class="style12" >
                <asp:Label ID="New_Password_Label" runat="server" Text="Nowe hasło:"></asp:Label>
            </td>
            <td class="style9">
                <asp:TextBox ID="New_Password" runat="server" TextMode="Password" Width="200px"> </asp:TextBox>
            </td>
            <td>
                <asp:Label ID="Password_info" runat="server" Text="Hasło powinno od 8 do 15 znaków."></asp:Label>
            </td>
        </tr>
        <tr>
            <td class="style12" >
                <asp:Label ID="New_Password_Conform_Label" runat="server" Text="Powtórz nowe hasło:"></asp:Label>
            </td>
            <td class="style9">
                <asp:TextBox ID="New_Password_Conform" runat="server" TextMode="Password" Width="200px"> </asp:TextBox>
            </td>
        </tr>
         <tr>
            <td>  </td>    
            <td id="Change_Password" align="right" style="margin-right: 30px;"> 
                <asp:Button ID="Change_Password_Button" runat="server" Text="Zmień hasło" OnClick="Password_Change" > </asp:Button>
            </td>    
        </tr>
    </table>   

</asp:Content>
