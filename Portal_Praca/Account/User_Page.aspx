﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" 
CodeBehind="User_Page.aspx.cs" Inherits="Portal_Praca.Account.User_Page" %>

<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server">

    <style type="text/css">
        .style1
        {
            width: 200px;
        }
        .style2
        {
            width: 200px;
        }
    </style>

</asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="MainContent" runat="server">
   
   <asp:Menu ID="Login_user_menu" runat="server" CssClass="user_menu" 
        EnableViewState="false" IncludeStyleBlock="false" Orientation="Vertical">
        
        <Items>
            
            <asp:MenuItem NavigateUrl="~/Account/User_Page.aspx" Text="Mój Profil" />
            
            <asp:MenuItem NavigateUrl="~/Account/Employee/Edit_Profile.aspx" Text="Edytuj swoje dane" Value="Employee_info" />
            <asp:MenuItem NavigateUrl="~/Account/Employer/Edit_Profile.aspx" Text="Edytuj swoje dane" Value="Employer_info" />
            
            <asp:MenuItem NavigateUrl="~/Account/Change_Password.aspx" Text="Zmień Hasło" />
            
        </Items>

</asp:Menu>

</asp:Content>


<asp:Content ID="Content2" ContentPlaceHolderID="left_content" runat="server">
    <table>
    <tr>
        <th class="style1">
            <asp:Label ID="Company_Name_Label" runat="server" Text="Nazwa Firmy:"></asp:Label>
            <asp:Label ID="Name_Label" runat="server" Text="Imie:"></asp:Label>
        </th>
        <td class="style2">
            <asp:Label ID="Company_Name_Content" runat="server"></asp:Label>
            <asp:Label ID="Name_Content" runat="server"></asp:Label>
        </td>
     </tr>

     <tr>
        <th class="style1">
            <asp:Label ID="Company_Address_Label" runat="server" Text="Adres Firmy:"></asp:Label>
            <asp:Label ID="Surname_Label" runat="server" Text="Nazwisko:"></asp:Label>
        </th>
        <td class="style2">
            <asp:Label ID="Company_Address_Content" runat="server"></asp:Label>
            <asp:Label ID="Surname_Content" runat="server" ></asp:Label>
        </td>
     </tr>

      <tr>
        <th class="style1">
            <asp:Label ID="City_Company_Label" runat="server" Text="Miasto:"></asp:Label>
            <asp:Label ID="Pesel_Label" runat="server" Text="Pesel:"></asp:Label>
        </th>
        <td class="style2">
            <asp:Label ID="City_Company_Content" runat="server"></asp:Label>
            <asp:Label ID="Pesel_Content" runat="server"></asp:Label>
        </td>
     </tr>

     <tr>
        <th class="style1">
            <asp:Label ID="CEO_Name_Label" runat="server" Text="Właściciel Firmy:"></asp:Label>
            <asp:Label ID="Adress_Label" runat="server" Text="Adres:"></asp:Label>
        </th>
        <td class="style2">
            <asp:Label ID="CEO_Name_Content" runat="server"></asp:Label>
            <asp:Label ID="Adress_Content" runat="server"></asp:Label>
        </td>
     </tr>

      <tr>
        <th class="style1">
            <asp:Label ID="REGON_Label" runat="server" Text="REGON:"></asp:Label>
            <asp:Label ID="City_Label" runat="server" Text="Miasto:"></asp:Label>
        </th>
        <td class="style2">
            <asp:Label ID="REGON_Content" runat="server"></asp:Label>
            <asp:Label ID="City_Content" runat="server"></asp:Label>
        </td>
     </tr>

      <tr>
        <th class="style1">
            <asp:Label ID="Looking_Occupation_Label" runat="server" Text="Poszukiwany/ne zawód/dy:"></asp:Label>
            <asp:Label ID="Phone_Label" runat="server" Text="Telefon:"></asp:Label>
        </th>
        <td class="style2">
            <asp:Label ID="Looking_Occupation_Content" runat="server"></asp:Label>
            <asp:Label ID="Phone_Content" runat="server"></asp:Label>
        </td>
     </tr>

      <tr>
        <th class="style1">
            <asp:Label ID="Occupation_Label" runat="server" Text="Zawód:"></asp:Label>
        </th>
        <td class="style2">
            <asp:Label ID="Occupation_Content" runat="server"></asp:Label>
        </td>
     </tr>
     
      <tr>
        <th class="style1">
            <asp:Label ID="Certyficate_Graduade_Label" runat="server" Text="Ukończona Szkoła/Uczelnia:"></asp:Label>
        </th>
        <td class="style2">
            <asp:Label ID="Certyficate_Graduade_Content" runat="server"></asp:Label>
        </td>
     </tr>

      <tr>
        <th class="style1">
            <asp:Label ID="Certyficate_Course_Label" runat="server" Text="Przebyte Kursy:"></asp:Label>
        </th>
        <td class="style2">
            <asp:Label ID="Certyficate_Course_Content" runat="server"></asp:Label>
        </td>
     </tr>

     <tr>
        <th class="style1">
            <asp:Label ID="Status_Label" runat="server" Text="Status:"></asp:Label>
        </th>
        <td class="style2">
            <asp:Label ID="Status_Content" runat="server"></asp:Label>
        </td>
     </tr>

     <tr>
        <th class="style1">
            <asp:Label ID="Additional_Info_Label" runat="server" Text="Informacje dodatkowe:"></asp:Label>
        </th>
        <td class="style2">
            <asp:Label ID="Additional_Info_Content" runat="server"></asp:Label>
        </td>
     </tr>

     </table>
</asp:Content>


