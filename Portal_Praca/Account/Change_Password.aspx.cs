﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.Security;
using System.Security.Cryptography;
using System.Text;
using System.Data.SqlClient;
using System.Text.RegularExpressions;


namespace Portal_Praca.Account
{
    public partial class Change_Password : System.Web.UI.Page
    {
        SqlConnection con;

        protected void Page_Load(object sender, EventArgs e)
        {
            if (Session["login"] == null && Session["IP"] != Request.ServerVariables["REMOTE_ADDR"])
            {
                Response.Redirect("~/Login/Login.aspx");
            }

            con = new SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings["Portal_pracaConnectionString"].ConnectionString);
        }


        protected void Password_Change(object sender, EventArgs e)
        {
            String actual_password_text;
            actual_password_text = FormsAuthentication.HashPasswordForStoringInConfigFile(Actual_Password.Text, "sha1");

            SqlCommand c1 = new SqlCommand("SELECT Password FROM Logins WHERE Login LIKE '" + Session["login"] + "' AND Password LIKE '" + actual_password_text + "'", con);

            String new_password, new_password_confirm;
            String pattern_password = "[a-zA-Z0-9]{8,15}";

            con.Open();
            SqlDataReader sdr2 = c1.ExecuteReader();

            try
            {
                if (Actual_Password.Text == "" || Actual_Password.Text == null || New_Password.Text == "" || New_Password.Text == null || New_Password_Conform.Text == "" || New_Password_Conform.Text == null)
                {
                    throw new Exception("Nie wszystkich żadnych pól");
                }
                else
                {
                    if (sdr2.HasRows == true)
                    {
                        new_password = New_Password.Text;
                        new_password_confirm = New_Password_Conform.Text;

                        if (new_password == new_password_confirm)
                        {
                            if(Regex.IsMatch(new_password, pattern_password) == false || Regex.IsMatch(new_password_confirm , pattern_password) == false )
                            {
                                Actual_Password_Label.ForeColor = System.Drawing.ColorTranslator.FromHtml("gray");
                                New_Password_Conform_Label.ForeColor = System.Drawing.ColorTranslator.FromHtml("red");
                                New_Password_Label.ForeColor = System.Drawing.ColorTranslator.FromHtml("red");
                                Password_info.ForeColor = System.Drawing.ColorTranslator.FromHtml("red");
                                throw new Exception("Podałeś zły format hasła.");
                            }
                            else
                            {
                                sdr2.Close();

                                new_password = FormsAuthentication.HashPasswordForStoringInConfigFile(new_password, "sha1");
                                Error_Label.Visible = false;
                                SqlCommand c2 = new SqlCommand("UPDATE Logins SET Password='" + new_password + "' WHERE Login LIKE '" + Session["login"] + "'", con);
                                c2.ExecuteNonQuery();
                                Response.Write("<script language=\"javascript\"> alert('Twoje hasło zostało zmienione.'); </script>");
                            }
                            
                        }
                        else
                        {
                            Actual_Password_Label.ForeColor = System.Drawing.ColorTranslator.FromHtml("gray");
                            Password_info.ForeColor = System.Drawing.ColorTranslator.FromHtml("gray");
                            New_Password_Conform_Label.ForeColor = System.Drawing.ColorTranslator.FromHtml("red");
                            New_Password_Label.ForeColor = System.Drawing.ColorTranslator.FromHtml("red");
                            throw new Exception("Podane Hasła nie są takie same.");
                        }
                    }
                    else
                    {
                        Password_info.ForeColor = System.Drawing.ColorTranslator.FromHtml("gray");
                        New_Password_Conform_Label.ForeColor = System.Drawing.ColorTranslator.FromHtml("gray");
                        New_Password_Label.ForeColor = System.Drawing.ColorTranslator.FromHtml("gray");
                        Actual_Password_Label.ForeColor = System.Drawing.ColorTranslator.FromHtml("red");
                        throw new Exception("Podałeś złe hasło.");
                    }
                }
            }
            catch (Exception a)
            {
                Error_Label.Visible = true;
                Error_Label.ForeColor = System.Drawing.ColorTranslator.FromHtml("red");
                Error_Label.Text = a.Message;

                sdr2.Close();
                con.Close();
            }
            con.Close();
        }
        
    }
}