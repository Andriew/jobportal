﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data.SqlClient;
using System.Data;
using System.Text.RegularExpressions;

namespace Portal_Praca.Account.Employer
{
    public partial class Edit_Profile : System.Web.UI.Page
    {
        //Response.Write("<script language=\"javascript\"> alert('" + string.IsNullOrEmpty(sdr2.GetValue(6).ToString()) + "'); </script>");
        SqlConnection con;
        int id;

        protected void Page_Load(object sender, EventArgs e)
        {
            if (Session["login"] == null && Session["IP"] != Request.ServerVariables["REMOTE_ADDR"] || Session["Type"] == "Employee")
            {
                Response.Redirect("~/Login/Login.aspx");
            }

            
            con = new SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings["Portal_pracaConnectionString"].ConnectionString);
    
            SqlCommand c1 = new SqlCommand("SELECT id FROM Logins WHERE Login LIKE '" + Session["login"] + "'", con);
            con.Open();
            id = int.Parse( c1.ExecuteScalar().ToString() );
            
            SqlCommand c2 = new SqlCommand("SELECT * FROM Employers WHERE Id_Logins LIKE '" + id + "'", con);
            SqlDataReader sdr2 = c2.ExecuteReader();

            while (sdr2.Read())
            {
                if (sdr2.GetValue(0) != null)
                {
                    if (!Page.IsPostBack)
                    {
                        Company_Name_Edit.Text = sdr2.GetValue(2).ToString(); //nazwa firmy
                        Company_Address_Edit.Text = sdr2.GetValue(3).ToString(); //adres firmy
                        City_Company_Edit.Text = sdr2.GetValue(4).ToString(); //miasto
                        CEO_Name_Edit.Text = sdr2.GetValue(5).ToString(); //CEO
                        REGON_Edit.Text = sdr2.GetValue(7).ToString(); //regon
                    }
                }
            }

            sdr2.Close();
            con.Close();
        }

        protected void save_change(object sender, EventArgs e)
        {
            string tmp = Company_Name_Edit.Text.Trim();
            try
            {
                validator(sender, e);

                con.Open();

                SqlCommand command = new SqlCommand("UPDATE Employers SET Company_Name=@company_name, Company_Address=@company_address, " +
                    "City=@city, CEO_Name=@ceo_name, REGON=@regon WHERE Id_Logins LIKE'" + id.ToString() + "'", con);
                
                command.Parameters.AddWithValue("@company_name", Company_Name_Edit.Text.Trim());
                command.Parameters.AddWithValue("@company_address", Company_Address_Edit.Text.Trim());
                command.Parameters.AddWithValue("@city", City_Company_Edit.Text.Trim());
                command.Parameters.AddWithValue("@ceo_name", Company_Address_Edit.Text.Trim());
                command.Parameters.AddWithValue("@regon", REGON_Edit.Text.Trim());

                command.ExecuteNonQuery();
                con.Close();

                Response.Write("<script language=\"javascript\"> alert('Dane zostały zapisane.');" +
                    "window.location.href=window.location.href; </script>");

            }
            catch (Exception a)
            {
                Error_Label.Visible = true;
                Error_Label.ForeColor = System.Drawing.ColorTranslator.FromHtml("red");
                Error_Label.Text = a.Message;
            }
            finally
            {
                Error_Label.Visible = false;
                con.Close();
            }
        }

        protected void validator(object sender, EventArgs e)
        {
            string regon_pattern = "^[0-9]{9}$";



            if (Company_Name_Edit.Text == null || Company_Name_Edit.Text == "")
            {
                Company_Name_Label.ForeColor = System.Drawing.ColorTranslator.FromHtml("red");
                throw new Exception("Nie wypełniłeś wszystkich pól.");
            }
            else
            {
                Error_Label.Visible = false;
                Company_Name_Label.ForeColor = System.Drawing.ColorTranslator.FromHtml("gray");
            }

            if (Company_Address_Edit.Text == null || Company_Address_Edit.Text == "")
            {
                Company_Address_Label.ForeColor = System.Drawing.ColorTranslator.FromHtml("red");
                throw new Exception("Nie wypełniłeś wszystkich pól.");
            }
            else
            {
                Error_Label.Visible = false;
                Company_Address_Label.ForeColor = System.Drawing.ColorTranslator.FromHtml("gray");
            }

            if (City_Company_Edit.Text == null || City_Company_Edit.Text == "")
            {
                City_Company_Label.ForeColor = System.Drawing.ColorTranslator.FromHtml("red");
                throw new Exception("Nie wypełniłeś wszystkich pól.");
            }
            else
            {
                Error_Label.Visible = false;
                City_Company_Label.ForeColor = System.Drawing.ColorTranslator.FromHtml("gray");
            }

            if (CEO_Name_Edit.Text == null || CEO_Name_Edit.Text == "")
            {
                CEO_Name_Label.ForeColor = System.Drawing.ColorTranslator.FromHtml("red");
                throw new Exception("Nie wypełniłeś wszystkich pól.");
            }
            else
            {
                Error_Label.Visible = false;
                CEO_Name_Label.ForeColor = System.Drawing.ColorTranslator.FromHtml("gray");
            }

            if (REGON_Edit.Text == null || REGON_Edit.Text == "")
            {
                REGON_Label.ForeColor = System.Drawing.ColorTranslator.FromHtml("red");
                throw new Exception("Nie wypełniłeś wszystkich pól.");
            }
            else
            {
                Error_Label.Visible = false;
                REGON_Label.ForeColor = System.Drawing.ColorTranslator.FromHtml("gray");
            }

            if (Regex.IsMatch(REGON_Edit.Text, regon_pattern) == false)
            {
                REGON_Label.ForeColor = System.Drawing.ColorTranslator.FromHtml("red");
                throw new Exception("Wprowadzony REGON jest nieprawidłowy.");
            }
            else
            {
                Error_Label.Visible = false;
                REGON_Label.ForeColor = System.Drawing.ColorTranslator.FromHtml("gray");
            }
        }


    }
}