﻿<%@ Page Title="Edytuj Profil" Language="C#"  MasterPageFile="Employer.Master"
AutoEventWireup="true" CodeBehind="Edit_Profile.aspx.cs" Inherits="Portal_Praca.Account.Employer.Edit_Profile" %>

<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server">
    <style type="text/css">
        .style1
        {
            width: 200px;
        }
        .style2
        {
            width: 250px;
        }
    </style>
   
</asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="left_content" runat="server">
 
<table>
 <tr>
        <th class="style1">
            <asp:Label ID="Error_Label" runat="server" Visible="false"></asp:Label>
        </th>
     </tr>
    <tr>
        <th class="style1">
            <asp:Label ID="Company_Name_Label" runat="server" Text="Nazwa Firmy:"></asp:Label>
        </th>
        <td class="style2">
            <asp:TextBox ID="Company_Name_Edit" Width="250px" runat="server" />
        </td>
     </tr>

     <tr>
        <th class="style1">
            <asp:Label ID="Company_Address_Label" runat="server" Text="Adres Firmy:"></asp:Label>
        </th>
        <td class="style2">
            <asp:TextBox ID="Company_Address_Edit" Width="250px" runat="server" />
        </td>
     </tr>

      <tr>
        <th class="style1">
            <asp:Label ID="City_Company_Label" runat="server" Text="Miasto:"></asp:Label>
        </th>
        <td class="style2">
            <asp:TextBox ID="City_Company_Edit" Width="250px" runat="server" />
        </td>
     </tr>

     <tr>
        <th class="style1">
            <asp:Label ID="CEO_Name_Label" runat="server" Text="Właściciel Firmy:"></asp:Label>
        </th>
        <td class="style2">
            <asp:TextBox ID="CEO_Name_Edit" Width="250px" runat="server" />
        </td>
     </tr>

      <tr>
        <th class="style1">
            <asp:Label ID="REGON_Label" runat="server" Text="REGON:"></asp:Label>
        </th>
        <td class="style2">
            <asp:TextBox ID="REGON_Edit" Width="250px" runat="server" />
        </td>
     </tr>
      <tr>
        <td colspan="2" align="center">
                <asp:Button ID="Looking_Occupation_Button" runat="server" Text="Zapisz Dane" OnClick="save_change" />
        </td>
     </tr>
     </table>

</asp:Content>
