﻿<%@ Page Title="Edytuj Profil" Language="C#" MasterPageFile="~/Account/Employee/Employee.Master" 
AutoEventWireup="true" CodeBehind="Edit_Profile.aspx.cs" Inherits="Portal_Praca.Account.Employee.Edit_Profile" %>

<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server">
    <style type="text/css">
        .style1
        {
            width: 200px;
        }
        .style2
        {
            width: 200px;
        }
    </style>
   
</asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="left_content" runat="server">
 <table>
 <tr>
        <td class="style1" colspan="2">
            <asp:Label ID="Error_Label" runat="server" Visible="false"></asp:Label>
        </td>
     </tr>
     <tr>
        <td colspan="2">
            <asp:Label ID="info" runat="server" Text="Pola z * są obowiązkowe"></asp:Label>
        </td>
     </tr>
    <tr>
        <th class="style1">
            <asp:Label ID="Name_Label" runat="server" Text="Imie:*"></asp:Label>
        </th>
        <td class="style2">
            <asp:TextBox ID="Name_Edit" Width="250px" runat="server" />
        </td>
     </tr>

     <tr>
        <th class="style1">
            <asp:Label ID="Surname_Label" runat="server" Text="Nazwisko:*"></asp:Label>
        </th>
        <td class="style2">
            <asp:TextBox ID="Surname_Edit" Width="250px" runat="server" />
        </td>
     </tr>

      <tr>
        <th class="style1">
            <asp:Label ID="Pesel_Label" runat="server" Text="Pesel:*"></asp:Label>
        </th>
        <td class="style2">
            <asp:TextBox ID="Pesel_Edit" Width="250px" runat="server" />
        </td>
     </tr>

     <tr>
        <th class="style1">
            <asp:Label ID="Address_Label" runat="server" Text="Adres:*"></asp:Label>
        </th>
        <td class="style2">
            <asp:TextBox ID="Address_Edit" Width="250px" runat="server" />
        </td>
     </tr>

      <tr>
        <th class="style1">
            <asp:Label ID="City_Label" runat="server" Text="Miasto:*"></asp:Label>
        </th>
        <td class="style2">
            <asp:TextBox ID="City_Edit" Width="250px" runat="server" />
        </td>
     </tr>

      <tr>
        <th class="style1">
            <asp:Label ID="Phone_Label" runat="server" Text="Telefon:"></asp:Label>
        </th>
        <td class="style2">
            <asp:TextBox ID="Phone_Edit" Width="250px" runat="server" />
        </td>
     </tr>

      <tr>
        <th class="style1">
            <asp:Label ID="Occupation_Label" runat="server" Text="Zawód:"></asp:Label>
        </th>
        <td class="style2">
            <asp:TextBox ID="Occupation_Edit" Width="250px" runat="server" />
        </td>
     </tr>
     
      <tr>
        <th class="style1">
            <asp:Label ID="Certyficate_Graduade_Label" runat="server" Text="Ukończona Szkoła/Uczelnia:"></asp:Label>
        </th>
        <td class="style2">
            <asp:TextBox ID="Certyficate_Graduade_Edit" Width="250px" runat="server" />
        </td>
     </tr>

      <tr>
        <th class="style1">
            <asp:Label ID="Certyficate_Course_Label" runat="server" Text="Przebyte Kursy:"></asp:Label>
        </th>
        <td class="style2">
            <asp:TextBox ID="Certyficate_Course_Edit" Width="250px" runat="server" />
        </td>
     </tr>

     <tr>
        <th class="style1">
            <asp:Label ID="Status_Label" runat="server" Text="Status:"></asp:Label>
        </th>
        <td class="style2">
            <asp:CheckBox ID="Status_Checkbox" runat="server" /> Aktywny/Nieaktywny
        </td>
     </tr>

     <tr>
        <th class="style1">
            <asp:Label ID="Additional_Info_Label" runat="server" Text="Informacje dodatkowe:"></asp:Label>
        </th>
        <td class="style2">
           <asp:TextBox ID="Additional_Info_Edit" Width="250px" runat="server" />
        </td>
     </tr>
   
        <tr>
        <td colspan="2" align="center" >
           <asp:Button ID="Employee_Edit_Button" runat="server" Text="Zapisz zmiany" OnClick="save_change" > </asp:Button>
        </td>
     </tr>

     </table>
</asp:Content>