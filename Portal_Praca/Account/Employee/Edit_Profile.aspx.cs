﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data.SqlClient;
using System.Data;
using System.Text.RegularExpressions;

namespace Portal_Praca.Account.Employee
{
    public partial class Edit_Profile : System.Web.UI.Page
    {
        //Response.Write("<script language=\"javascript\"> alert('" + string.IsNullOrEmpty(sdr2.GetValue(6).ToString()) + "'); </script>");
        SqlConnection con;
        int id;

        protected void Page_Load(object sender, EventArgs e)
        {
            if (Session["login"] == null && Session["IP"] != Request.ServerVariables["REMOTE_ADDR"] || Session["Type"] == "Employer")
            {
                Response.Redirect("~/Login/Login.aspx");
            }


            con = new SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings["Portal_pracaConnectionString"].ConnectionString);

            SqlCommand c1 = new SqlCommand("SELECT id FROM Logins WHERE Login LIKE '" + Session["login"] + "'", con);
            con.Open();
            id = int.Parse(c1.ExecuteScalar().ToString());

            SqlCommand c2 = new SqlCommand("SELECT * FROM Employees WHERE Id_Logins LIKE '" + id + "'", con);
            SqlDataReader sdr2 = c2.ExecuteReader();

            while (sdr2.Read())
            {
                if (sdr2.GetValue(0) != null)
                {
                    if (!Page.IsPostBack)
                    {
                        Name_Edit.Text = sdr2.GetValue(2).ToString(); //imie
                        Surname_Edit.Text = sdr2.GetValue(3).ToString(); //Nazwisko
                        Address_Edit.Text = sdr2.GetValue(4).ToString(); //adres
                        City_Edit.Text = sdr2.GetValue(5).ToString(); //miasto
                        Phone_Edit.Text = sdr2.GetValue(6).ToString(); //telefon
                        Occupation_Edit.Text = sdr2.GetValue(7).ToString(); //zawod
                        Status_Checkbox.Checked = bool.Parse( sdr2.GetValue(8).ToString() ); //status
                        Certyficate_Course_Edit.Text = sdr2.GetValue(10).ToString(); //kursy
                        Certyficate_Graduade_Edit.Text = sdr2.GetValue(12).ToString(); //skonczona szkola
                        Pesel_Edit.Text = sdr2.GetValue(13).ToString(); //pesel
                        Additional_Info_Edit.Text = sdr2.GetValue(14).ToString(); //informacje dodatkowe

                    }
                }
            }

            sdr2.Close();
            con.Close();
        }

        protected void save_change(object sender, EventArgs e)
        {
            try
            {
                validator(sender, e);

                con.Open();

                SqlCommand command = new SqlCommand("UPDATE Employees SET Name=@name, Surname=@surname, Address=@address, " +
                    "City=@city, Phone=@phone, Occupation=@occupation, Status=@status, Certyficate_Course=@certyficate_course, " +
                    "Certyficate_Graduade=@certyficate_graduade, Pesel=@pesel, Additional_Occupation=@additional_ocupation " +
                    "WHERE Id_Logins LIKE'" + id.ToString() + "'", con);
                
                
                command.Parameters.AddWithValue("@name", Name_Edit.Text.Trim());
                command.Parameters.AddWithValue("@surname", Surname_Edit.Text.Trim());
                command.Parameters.AddWithValue("@address", Address_Edit.Text.Trim());
                command.Parameters.AddWithValue("@city", City_Edit.Text.Trim());
                command.Parameters.AddWithValue("@phone", Phone_Edit.Text.Trim());
                command.Parameters.AddWithValue("@occupation", Occupation_Edit.Text.Trim());
                command.Parameters.AddWithValue("@status", Status_Checkbox.Checked );
                command.Parameters.AddWithValue("@certyficate_course", Certyficate_Course_Edit.Text.Trim());
                command.Parameters.AddWithValue("@certyficate_graduade", Certyficate_Graduade_Edit.Text.Trim());
                command.Parameters.AddWithValue("@pesel", Pesel_Edit.Text.Trim());
                command.Parameters.AddWithValue("@additional_ocupation", Additional_Info_Edit.Text.Trim());
                
                command.ExecuteNonQuery();
                con.Close();

                Response.Write("<script language=\"javascript\"> alert('Dane zostały zapisane.');" +
                    "window.location.href=window.location.href; </script>");

            }
            catch (Exception a)
            {
                Error_Label.Visible = true;
                Error_Label.ForeColor = System.Drawing.ColorTranslator.FromHtml("red");
                Error_Label.Text = a.Message;
            }
            finally
            {
                Error_Label.Visible = false;
                con.Close();
            }
        }

        protected void validator(object sender, EventArgs e)
        {
            string Pesel_pattern = "^[0-9]{11}$";
            
            //imie
            if (Name_Edit.Text == null || Name_Edit.Text == "")
            {
                Name_Label.ForeColor = System.Drawing.ColorTranslator.FromHtml("red");
                throw new Exception("Nie wypełniłeś wszystkich pól.");
            }
            else
            {
                Error_Label.Visible = false;
                Name_Label.ForeColor = System.Drawing.ColorTranslator.FromHtml("gray");
            }

            //nazwisko
            if (Surname_Edit.Text == null || Surname_Edit.Text == "")
            {
                Surname_Label.ForeColor = System.Drawing.ColorTranslator.FromHtml("red");
                throw new Exception("Nie wypełniłeś wszystkich pól.");
            }
            else
            {
                Error_Label.Visible = false;
                Surname_Label.ForeColor = System.Drawing.ColorTranslator.FromHtml("gray");
            }

            //pesel
            if (Pesel_Edit.Text == null || Pesel_Edit.Text == "")
            {
                Pesel_Label.ForeColor = System.Drawing.ColorTranslator.FromHtml("red");
                throw new Exception("Nie wypełniłeś wszystkich pól.");
            }
            else
            {
                Error_Label.Visible = false;
                Pesel_Label.ForeColor = System.Drawing.ColorTranslator.FromHtml("gray");
            }

            //adres
            if (Address_Edit.Text == null || Address_Edit.Text == "")
            {
                Address_Label.ForeColor = System.Drawing.ColorTranslator.FromHtml("red");
                throw new Exception("Nie wypełniłeś wszystkich pól.");
            }
            else
            {
                Error_Label.Visible = false;
                Address_Edit.ForeColor = System.Drawing.ColorTranslator.FromHtml("gray");
            }

            //miasto
            if (City_Edit.Text == null || City_Edit.Text == "")
            {
                City_Label.ForeColor = System.Drawing.ColorTranslator.FromHtml("red");
                throw new Exception("Nie wypełniłeś wszystkich pól.");
            }
            else
            {
                Error_Label.Visible = false;
                City_Label.ForeColor = System.Drawing.ColorTranslator.FromHtml("gray");
            }

            //poprawny pesel
            if (Regex.IsMatch(Pesel_Edit.Text, Pesel_pattern) == false)
            {
                Pesel_Label.ForeColor = System.Drawing.ColorTranslator.FromHtml("red");
                throw new Exception("Wprowadzony REGON jest nieprawidłowy.");
            }
            else
            {
                Error_Label.Visible = false;
                Pesel_Label.ForeColor = System.Drawing.ColorTranslator.FromHtml("gray");
            }
            

        }


    }
}