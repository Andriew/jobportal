﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data.SqlClient;


namespace Portal_Praca.Account
{
    public partial class User_Page : System.Web.UI.Page
    {
        SqlConnection con;

        //Response.Write("<script language=\"javascript\"> alert('" + string.IsNullOrEmpty(sdr2.GetValue(6).ToString()) + "'); </script>");

        protected void Page_Load(object sender, EventArgs e)
        {
            if (Session["login"] == null && Session["IP"] != Request.ServerVariables["REMOTE_ADDR"] )
            {
                Response.Redirect("~/Login/Login.aspx");
            }

            con = new SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings["Portal_pracaConnectionString"].ConnectionString);

            //menu dla pracodawca !!!
            if (Session["type"].ToString() != "Employee")
            {
                //ukryj itemy pracownika
                this.hide_employee_items(sender, e);

                //zaladuj itemy pracodawcy
                this.load_employer_profile(sender, e);
            }

            //menu dla pracownik !!!
            if (Session["type"].ToString() != "Employer")
            {
                //ukryj itemy pracodawcy
                this.hide_employer_items(sender, e);

                //zaladuj itemy pracownika
                this.load_employee_profile(sender, e);
            }
            
        }

        protected void load_employee_profile(object sender, EventArgs e)
        {
            int id;
            
            SqlCommand c1 = new SqlCommand("SELECT id FROM Logins WHERE Login LIKE '" + Session["login"] +"'" , con);
            con.Open();
            id = int.Parse( c1.ExecuteScalar().ToString() );
            Name_Content.Text = id.ToString();
            
            SqlCommand c2 = new SqlCommand("SELECT * FROM Employees WHERE Id_Logins LIKE '" + id + "'", con);

            SqlDataReader sdr2 = c2.ExecuteReader();

            while (sdr2.Read())
            {
                if (sdr2.GetValue(0) != null)
                {
                    Name_Content.Text = sdr2.GetValue(2).ToString(); //imie
                    Surname_Content.Text = sdr2.GetValue(3).ToString(); //nazwisko
                    Adress_Content.Text = sdr2.GetValue(4).ToString(); //adres
                    City_Content.Text = sdr2.GetValue(5).ToString(); //miasto
                    
                    //Response.Write("<script language=\"javascript\"> alert('" + string.IsNullOrEmpty(sdr2.GetValue(6).ToString()) + "'); </script>");

                    //telefon
                    if ( string.IsNullOrEmpty(sdr2.GetValue(6).ToString()) == true ) 
                    {
                        Phone_Content.Font.Italic = true;
                        Phone_Content.Text = "Nie podano";
                    }
                    else
                        Phone_Content.Text = sdr2.GetValue(6).ToString();
                    
                    //zawod
                    if (string.IsNullOrEmpty(sdr2.GetValue(7).ToString()) == true) 
                    {
                        Occupation_Content.Font.Italic = true;
                        Occupation_Content.Text = "Nie podano";
                    }
                    else
                        Occupation_Content.Text = sdr2.GetValue(7).ToString();

                    //status
                    if (bool.Parse(sdr2.GetValue(8).ToString()) == false)
                    {
                        Status_Content.Font.Italic = true;
                        Status_Content.Text = "Nie aktywny";
                    }
                    else
                        Status_Content.Text = "Aktywny";

                    //certyfikaty kursy
                    if (string.IsNullOrEmpty(sdr2.GetValue(10).ToString()) == true) 
                    {
                        Certyficate_Course_Content.Font.Italic = true;
                        Certyficate_Course_Content.Text = "Nie podano";
                    }
                    else
                        Certyficate_Course_Content.Text = sdr2.GetValue(10).ToString();

                    //ukonczona szkola/uczelnia
                    if (string.IsNullOrEmpty(sdr2.GetValue(12).ToString()) == true) 
                    {
                        Certyficate_Graduade_Content.Font.Italic = true;
                        Certyficate_Graduade_Content.Text = "Nie podano";
                    }
                    else
                        Certyficate_Graduade_Content.Text = sdr2.GetValue(12).ToString();

                    //Pesel
                    if (string.IsNullOrEmpty(sdr2.GetValue(13).ToString()) == true) 
                    {
                        Pesel_Content.Font.Italic = true;
                        Pesel_Content.Text = "Nie podano";
                    }
                    else
                        Pesel_Content.Text = sdr2.GetValue(13).ToString();

                    //Informacje dodatkowe
                    if (string.IsNullOrEmpty(sdr2.GetValue(14).ToString()) == true) 
                    {
                        Additional_Info_Content.Font.Italic = true;
                        Additional_Info_Content.Text = "Nie podano";
                    }
                    else
                        Additional_Info_Content.Text = sdr2.GetValue(14).ToString();


                }
            }
            sdr2.Close();
            con.Close();
        }
         
        protected void hide_employee_items(object sender, EventArgs e)
        {
            //itemy menu
            Login_user_menu.Items.Remove(Login_user_menu.FindItem("Employee_info"));

            //itemy profilu
            Name_Label.Visible = false;
            Name_Content.Visible = false;

            Surname_Label.Visible = false;
            Surname_Content.Visible = false;

            Pesel_Label.Visible = false;
            Pesel_Content.Visible = false;

            Adress_Label.Visible = false;
            Adress_Content.Visible = false;

            City_Label.Visible = false;
            City_Content.Visible = false;

            Phone_Label.Visible = false;
            Phone_Content.Visible = false;

            Occupation_Label.Visible = false;
            Occupation_Content.Visible = false;

            Certyficate_Course_Label.Visible = false;
            Certyficate_Course_Content.Visible = false;

            Certyficate_Graduade_Label.Visible = false;
            Certyficate_Graduade_Content.Visible = false;

            Status_Label.Visible = false;
            Status_Content.Visible = false;

            Additional_Info_Label.Visible = false;
            Additional_Info_Content.Visible = false;
        }
            
        protected void hide_employer_items(object sender, EventArgs e)
        {
            //itemy menu
            Login_user_menu.Items.Remove(Login_user_menu.FindItem("Employer_info"));

            //itemy profilu
            Company_Name_Label.Visible = false;
            Company_Name_Content.Visible = false;

            Company_Address_Label.Visible = false;
            Company_Address_Content.Visible = false;

            CEO_Name_Label.Visible = false;
            CEO_Name_Content.Visible = false;

            Looking_Occupation_Label.Visible = false;
            Looking_Occupation_Content.Visible = false;

            REGON_Label.Visible = false;
            REGON_Content.Visible = false;
        }

        protected void load_employer_profile(object sender, EventArgs e)
        {
            int id;
            
            SqlCommand c1 = new SqlCommand("SELECT id FROM Logins WHERE Login LIKE '" + Session["login"] +"'" , con);
            con.Open();
            id = int.Parse( c1.ExecuteScalar().ToString() );
            Company_Name_Content.Text = id.ToString();
            
            SqlCommand c2 = new SqlCommand("SELECT * FROM Employers WHERE Id_Logins LIKE '" + id + "'", con);

            SqlDataReader sdr2 = c2.ExecuteReader();

            while (sdr2.Read())
            {
                if (sdr2.GetValue(0) != null)
                {
                    Company_Name_Content.Text = sdr2.GetValue(2).ToString(); //nazwa firmy
                    Company_Address_Content.Text = sdr2.GetValue(3).ToString(); //adres firmy

                    City_Company_Content.Text = sdr2.GetValue(4).ToString(); //miasto
                    CEO_Name_Content.Text = sdr2.GetValue(5).ToString(); //CEO

                    REGON_Content.Text = sdr2.GetValue(7).ToString(); //regon

                    //poszukiwany zawod/zawody
                    if (string.IsNullOrEmpty(sdr2.GetValue(6).ToString()) == true || sdr2.GetValue(6) == null)
                    {
                        Looking_Occupation_Content.Font.Italic = true;
                        Looking_Occupation_Content.Text = "Nie podano";
                    }
                    else
                        Looking_Occupation_Content.Text = sdr2.GetValue(5).ToString();

                }
            }

            sdr2.Close();
            con.Close();


        }

    }
}