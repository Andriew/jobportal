﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Portal_Praca.Account
{
    public partial class Logout : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (Session["Login"] != null)
            {
                Session.Abandon();
                Response.Write("<meta http-equiv=\"refresh\" content=\"2;url=../Default.aspx\">");
            }
            else
            {
                Response.Redirect("~/Default.aspx");
            }

        }
    }
}