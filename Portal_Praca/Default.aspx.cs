﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data.SqlClient;

namespace Potal_Praca
{
    public partial class _Default : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            SqlConnection con;

            if (Request["token"] != null && Request["option"] != null)
            {
                if (Request["option"] == "activate")
                {
                    Response.Redirect("Register/Register_Activate.aspx?token=" + Request["token"].ToString());
                }
                if(Request["option"] == "forgotten_password")
                {
                    Response.Redirect("Login/Forgotten_Password_Activate.aspx?token=" + Request["token"].ToString());
                }
                if(Request["option"] == "forgotten_login")
                {
                    Response.Redirect("Login/Forgotten_Login_Activate.aspx?token=" + Request["token"].ToString());
                }

                HttpCookie myCookie = Request.Cookies["myCookie"];
                if (myCookie != null)
                {
                    String token, login, password;

                    if ( !string.IsNullOrEmpty(myCookie.Values["token"]) && !string.IsNullOrEmpty(myCookie.Values["login"]) && !string.IsNullOrEmpty(myCookie.Values["password"]) )
                    {
                        login = myCookie.Values["login"].ToString();
                        password = myCookie.Values["password"].ToString();
                        token = myCookie.Values["token"].ToString();

                        //Response.Redirect("Login/Login.aspx?token=" + Request["token"].ToString());
                        
                        con = new SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings["Portal_pracaConnectionString"].ConnectionString);

                        SqlCommand c1 = new SqlCommand("SELECT * FROM Logins WHERE Login LIKE '" + login + "' AND Password LIKE '" + password + "' AND Token LIKE '" + token +"'", con);
                        con.Open();

                        if (c1.ExecuteScalar() != null)
                        {
                            Session["Login"] = login;
                            Session["Password"] = password;
                            Session["Token"] = token;
                        }

                        con.Close();

                        Response.Redirect("Login/Login.aspx");
                    }
                }
            }
                
            if ( Session["Login"] != null )
            {
                Response.Redirect("~/Account/User_Page.aspx");
            }
            


            
        }
   }
}
