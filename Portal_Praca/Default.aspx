﻿<%@ Page Title="Strona Główna - Portal Praca" Language="C#" MasterPageFile="~/Site.master" AutoEventWireup="true"
    CodeBehind="Default.aspx.cs" Inherits="Potal_Praca._Default" %>

<asp:Content ID="HeaderContent" runat="server" ContentPlaceHolderID="HeadContent">
</asp:Content>

<asp:Content ID="BodyContent" runat="server" ContentPlaceHolderID="MainContent">
    <h2>
        Witaj na naszej stronie!
    </h2>
    <p>
        Jeśli jesteś zarejestrowany <a href="Login/Login.aspx">zaloguj się.</a>
    </p>
    <p>
        Jeśli jesteś tutaj pierwszy raz <a href="Register/Register.aspx">zarejestruj się.</a>
    </p>
    <br />
    
</asp:Content>
