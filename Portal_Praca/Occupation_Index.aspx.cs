﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data.SqlClient;


namespace Portal_Praca
{
    public partial class Occupation_Index : System.Web.UI.Page
    {
        SqlConnection con;

        //Response.Write("<script language=\"javascript\"> alert('" + + "'); </script>");

        protected void Page_Load(object sender, EventArgs e)
        {
            con = new SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings["Portal_pracaConnectionString"].ConnectionString);

            List<TableRow> tr = new List<TableRow>();
            int it = 0;

            SqlCommand c1 = new SqlCommand("SELECT Number, Description, Edu FROM Occupations" , con);
            con.Open();
            
            SqlDataReader sdr1 = c1.ExecuteReader();

            TableRow naglowek = new TableRow();
            Table1.Rows.Add(naglowek);

            TableCell index = new TableCell();
            index.Text = "Index zawodu";
            index.Style["font-weight"] = "bold";
            naglowek.Controls.Add(index);

            TableCell description = new TableCell();
            description.Text = "Opis";
            description.Style["font-weight"] = "bold";
            naglowek.Controls.Add(description);

            TableCell s = new TableCell();
            s.Text = "zawody szkolne ujęte w klasyfikacji zawodów szkolnictwa zawodowego";
            s.Style["font-weight"] = "bold";
            naglowek.Controls.Add(s);

            while (sdr1.Read())
            {
                if (sdr1.GetValue(0) != null)
                {
                    tr.Add(new TableRow() );
                    Table1.Rows.Add( tr[it] );

                    TableCell tc1 = new TableCell();
                    tc1.Text = sdr1.GetValue(0).ToString();
                    tr[it].Controls.Add(tc1);

                    TableCell tc2 = new TableCell();
                    tc2.Text = sdr1.GetValue(1).ToString();
                    tr[it].Controls.Add(tc2);

                    TableCell tc3 = new TableCell();
                    if (bool.Parse( sdr1.GetValue(2).ToString() ) == true)
                        tc3.Text = "Tak";
                    else
                        tc3.Text = "Nie";
                    tc3.Style["text-align"] = "right";
                    tr[it].Controls.Add(tc3);



                }
                it++;
            }
        }



    }
}