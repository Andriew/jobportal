﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

/*
 * Lista rzeczy zakomentowanych przy testowaniu:
 * odkomentowac wysylanie meila w pliku register_final(54)
 * odkomentowac validator w pliku employee_register(60) oraz emloyer_register(56)
 * 
 * 
*/

namespace Potal_Praca
{
    public partial class SiteMaster : System.Web.UI.MasterPage
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (Session["Login"] == null)
            {
                Login_box.Text = "<a href=\"/Login/Login.aspx\">Logowanie</a> "
                                    + "<a href=\"/Register/Register.aspx\">Zarejestruj</a> ";
                //"<asp:HyperLink target=\"~/Account/Login.aspx\" ID=\"Login_link\" runat=\"server\" text=\"Zaloguj \" /> "
                //+"<asp:HyperLink target=\"~/Register/Register.aspx\" ID=\"Register_link\" Text=\"Zarejestruj\" runat=\"server\" />";

            }
            else
            {
                Login_box.Text = "Witaj " + Session["Login"] + "!  "
                    + "<a href=\"/Account/Logout.aspx\">Wyloguj</a>";
                //+ "<asp:HyperLink target=\"~/Account/Logout.aspx\" ID=\"Logout_link\" Text=\"Wyloguj\" runat=\"server\" />";

            }

        }
    }
}


